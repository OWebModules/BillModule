﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class Voucher
    {
        public string IdReference { get; set; }
        public DateTime DateStamp { get; set; }
        public string Name { get; set; }

        public List<VoucherPosition> Positions { get; set; } = new List<VoucherPosition>();
    }
}
