﻿using BillModule.Models;
using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BillModule
{
    public class clsLocalConfig
    {
        private const string cstrID_Ontology = "733ae7fe72cc495b95004266edcab296";
        private XMLImportWorker objImport;

        public Globals Globals { get; set; }

        private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
        public clsOntologyItem OItem_BaseConfig { get; set; }

        private OntologyModDBConnector objDBLevel_Config1;
        private OntologyModDBConnector objDBLevel_Config2;

        public clsOntologyItem OItem_attribute_amount { get; set; }
        public clsOntologyItem OItem_attribute_beg_nstigter_zahlungspflichtiger { get; set; }
        public clsOntologyItem OItem_attribute_betrag { get; set; }
        public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
        public clsOntologyItem OItem_attribute_gross { get; set; }
        public clsOntologyItem OItem_attribute_gross__standard_ { get; set; }
        public clsOntologyItem OItem_attribute_menge { get; set; }
        public clsOntologyItem OItem_attribute_part____ { get; set; }
        public clsOntologyItem OItem_attribute_percent { get; set; }
        public clsOntologyItem OItem_attribute_to_pay { get; set; }
        public clsOntologyItem OItem_attribute_transaction_date { get; set; }
        public clsOntologyItem OItem_attribute_transaction_id { get; set; }
        public clsOntologyItem OItem_attribute_valutatag { get; set; }
        public clsOntologyItem OItem_attribute_zahlungsausgang { get; set; }
        public clsOntologyItem OItem_relationtype_accountings { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_amount { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_contractee { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_contractor { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_currency { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_payment { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_sem_item { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_tax_rate { get; set; }
        public clsOntologyItem OItem_relationtype_belongsto { get; set; }
        public clsOntologyItem OItem_relationtype_contains { get; set; }
        public clsOntologyItem OItem_relationtype_gegenkonto { get; set; }
        public clsOntologyItem OItem_relationtype_is_of_type { get; set; }
        public clsOntologyItem OItem_relationtype_isdescribedby { get; set; }
        public clsOntologyItem OItem_relationtype_located_in { get; set; }
        public clsOntologyItem OItem_relationtype_offered_by { get; set; }
        public clsOntologyItem OItem_relationtype_offers { get; set; }
        public clsOntologyItem OItem_relationtype_standard { get; set; }
        public clsOntologyItem OItem_relationtype_zugeh_rige_mandanten { get; set; }
        public clsOntologyItem OItem_token_direction_down { get; set; }
        public clsOntologyItem OItem_token_direction_up { get; set; }
        public clsOntologyItem OItem_token_module_bill_module { get; set; }
        public clsOntologyItem OItem_token_search_template_amount_ { get; set; }
        public clsOntologyItem OItem_token_search_template_contractee_contractor_ { get; set; }
        public clsOntologyItem OItem_token_search_template_name_ { get; set; }
        public clsOntologyItem OItem_token_search_template_payment_date_ { get; set; }
        public clsOntologyItem OItem_token_search_template_related_sem_item_ { get; set; }
        public clsOntologyItem OItem_token_search_template_to_pay_ { get; set; }
        public clsOntologyItem OItem_token_search_template_transaction_date_ { get; set; }
        public clsOntologyItem OItem_token_software_development_bill_module { get; set; }
        public clsOntologyItem OItem_type_bank_transaktionen__sparkasse_ { get; set; }
        public clsOntologyItem OItem_type_bank_transaktionen__sparkasse____archiv { get; set; }
        public clsOntologyItem OItem_type_bankleitzahl { get; set; }
        public clsOntologyItem OItem_type_beleg { get; set; }
        public clsOntologyItem OItem_type_belegsart { get; set; }
        public clsOntologyItem OItem_type_bill_module { get; set; }
        public clsOntologyItem OItem_type_container__physical_ { get; set; }
        public clsOntologyItem OItem_type_currencies { get; set; }
        public clsOntologyItem OItem_type_einheit { get; set; }
        public clsOntologyItem OItem_type_financial_transaction { get; set; }
        public clsOntologyItem OItem_type_financial_transaction___archive { get; set; }
        public clsOntologyItem OItem_type_kontonummer { get; set; }
        public clsOntologyItem OItem_type_language { get; set; }
        public clsOntologyItem OItem_type_menge { get; set; }
        public clsOntologyItem OItem_type_module { get; set; }
        public clsOntologyItem OItem_type_offset { get; set; }
        public clsOntologyItem OItem_type_partner { get; set; }
        public clsOntologyItem OItem_type_payment { get; set; }
        public clsOntologyItem OItem_type_search_template { get; set; }
        public clsOntologyItem OItem_type_tax_rates { get; set; }

        public BaseConfig BaseConfig { get; private set; }

        private void get_Data_DevelopmentConfig()
        {
            var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

            var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (objDBLevel_Config1.ObjectRels.Any())
                {

                    objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                    }).ToList();

                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingClass.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingObject.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                    }));

                    objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                    {
                        if (!objDBLevel_Config2.ObjectRels.Any())
                        {
                            throw new Exception("Config-Error");
                        }
                    }
                    else
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }

            }

        }

        public void GetBaseConfig()
        {
            var searchBaseConfig = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = OItem_token_module_bill_module.GUID,
                    ID_RelationType = Globals.RelationType_belongsTo.GUID,
                    ID_Parent_Object = OItem_type_bill_module.GUID
                }
            };

            var result = objDBLevel_Config1.GetDataObjectRel(searchBaseConfig);

            if (result.GUID == Globals.LState_Error.GUID || !objDBLevel_Config1.ObjectRels.Any())
            {
                throw new Exception("Config-Error");
            }

            OItem_BaseConfig = objDBLevel_Config1.ObjectRels.Select(rel => new clsOntologyItem
            {
                GUID = rel.ID_Object,
                Name = rel.Name_Object,
                GUID_Parent = rel.ID_Parent_Object,
                Type  = Globals.Type_Object
            }).FirstOrDefault();

            var searchGross = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = OItem_BaseConfig.GUID,
                        ID_AttributeType = OItem_attribute_gross__standard_.GUID
                    }
                };

            var dbReaderGross = new OntologyModDBConnector(Globals);

            BaseConfig = new BaseConfig();

            result = dbReaderGross.GetDataObjectAtt(searchGross);

            if (result.GUID == Globals.LState_Error.GUID || !dbReaderGross.ObjAtts.Any())
            {
                throw new Exception("Config-Error");
            }

            BaseConfig.BaseConfigToGrossStandard = dbReaderGross.ObjAtts.FirstOrDefault();

            var searchCurrency = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = OItem_BaseConfig.GUID,
                    ID_RelationType = OItem_relationtype_standard.GUID,
                    ID_Parent_Other = OItem_type_currencies.GUID
                }
            };

            var dbReaderStandardCurrency = new OntologyModDBConnector(Globals);

            result = dbReaderStandardCurrency.GetDataObjectRel(searchCurrency);

            if (result.GUID == Globals.LState_Error.GUID || !dbReaderStandardCurrency.ObjectRels.Any())
            {
                throw new Exception("Config-Error");
            }

            BaseConfig.BaseConfigToCurrency = dbReaderStandardCurrency.ObjectRels.FirstOrDefault();

            var searchUnit = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = OItem_BaseConfig.GUID,
                    ID_RelationType = OItem_relationtype_standard.GUID,
                    ID_Parent_Other = OItem_type_einheit.GUID
                }
            };

            var dbReaderStandardUnit = new OntologyModDBConnector(Globals);

            result = dbReaderStandardUnit.GetDataObjectRel(searchUnit);

            if (result.GUID == Globals.LState_Error.GUID || !dbReaderStandardUnit.ObjectRels.Any())
            {
                throw new Exception("Config-Error");
            }

            BaseConfig.BaseConfigToUnit = dbReaderStandardUnit.ObjectRels.FirstOrDefault();

            var searchMandanten = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = OItem_BaseConfig.GUID,
                    ID_RelationType = OItem_relationtype_zugeh_rige_mandanten.GUID,
                    ID_Parent_Other = OItem_type_partner.GUID
                }
            };

            var dbReaderMandanten = new OntologyModDBConnector(Globals);

            result = dbReaderMandanten.GetDataObjectRel(searchMandanten);

            if (result.GUID == Globals.LState_Error.GUID || !dbReaderMandanten.ObjectRels.Any())
            {
                throw new Exception("Config-Error");
            }

            BaseConfig.BaseConfigToMandanten = dbReaderMandanten.ObjectRels;
        }

        public clsLocalConfig()
        {
            Globals = new Globals();
            set_DBConnection();
            get_Config();
        }

        public clsLocalConfig(Globals Globals)
        {
            this.Globals = Globals;
            set_DBConnection();
            get_Config();
        }

        private void set_DBConnection()
        {
            objDBLevel_Config1 = new OntologyModDBConnector(Globals);
            objDBLevel_Config2 = new OntologyModDBConnector(Globals);
            objImport = new XMLImportWorker(Globals);
        }

        private void get_Config()
        {
            try
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
                GetBaseConfig();
            }
            catch (Exception ex)
            {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1)
                {
                    strTitle = objCustomAttributes.First().Title;
                }
                var objOItem_Result = objImport.ImportTemplates(objAssembly);
                if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                {
                    get_Data_DevelopmentConfig();
                    get_Config_AttributeTypes();
                    get_Config_RelationTypes();
                    get_Config_Classes();
                    get_Config_Objects();
                    GetBaseConfig();
                }
                else
                {
                    throw new Exception("Config not importable");
                }
                
            }
        }

        private void get_Config_AttributeTypes()
        {
            var objOList_attribute_amount = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "attribute_amount".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                             select objRef).ToList();

            if (objOList_attribute_amount.Any())
            {
                OItem_attribute_amount = new clsOntologyItem()
                {
                    GUID = objOList_attribute_amount.First().ID_Other,
                    Name = objOList_attribute_amount.First().Name_Other,
                    GUID_Parent = objOList_attribute_amount.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_beg_nstigter_zahlungspflichtiger = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                       where objOItem.ID_Object == cstrID_Ontology
                                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                       where objRef.Name_Object.ToLower() == "attribute_beg_nstigter_zahlungspflichtiger".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                                       select objRef).ToList();

            if (objOList_attribute_beg_nstigter_zahlungspflichtiger.Any())
            {
                OItem_attribute_beg_nstigter_zahlungspflichtiger = new clsOntologyItem()
                {
                    GUID = objOList_attribute_beg_nstigter_zahlungspflichtiger.First().ID_Other,
                    Name = objOList_attribute_beg_nstigter_zahlungspflichtiger.First().Name_Other,
                    GUID_Parent = objOList_attribute_beg_nstigter_zahlungspflichtiger.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_betrag = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "attribute_betrag".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                             select objRef).ToList();

            if (objOList_attribute_betrag.Any())
            {
                OItem_attribute_betrag = new clsOntologyItem()
                {
                    GUID = objOList_attribute_betrag.First().ID_Other,
                    Name = objOList_attribute_betrag.First().Name_Other,
                    GUID_Parent = objOList_attribute_betrag.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

            if (objOList_attribute_dbpostfix.Any())
            {
                OItem_attribute_dbpostfix = new clsOntologyItem()
                {
                    GUID = objOList_attribute_dbpostfix.First().ID_Other,
                    Name = objOList_attribute_dbpostfix.First().Name_Other,
                    GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_gross = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_gross".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

            if (objOList_attribute_gross.Any())
            {
                OItem_attribute_gross = new clsOntologyItem()
                {
                    GUID = objOList_attribute_gross.First().ID_Other,
                    Name = objOList_attribute_gross.First().Name_Other,
                    GUID_Parent = objOList_attribute_gross.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_gross__standard_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "attribute_gross__standard_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                       select objRef).ToList();

            if (objOList_attribute_gross__standard_.Any())
            {
                OItem_attribute_gross__standard_ = new clsOntologyItem()
                {
                    GUID = objOList_attribute_gross__standard_.First().ID_Other,
                    Name = objOList_attribute_gross__standard_.First().Name_Other,
                    GUID_Parent = objOList_attribute_gross__standard_.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_menge = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_menge".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

            if (objOList_attribute_menge.Any())
            {
                OItem_attribute_menge = new clsOntologyItem()
                {
                    GUID = objOList_attribute_menge.First().ID_Other,
                    Name = objOList_attribute_menge.First().Name_Other,
                    GUID_Parent = objOList_attribute_menge.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_part____ = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attribute_part____".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

            if (objOList_attribute_part____.Any())
            {
                OItem_attribute_part____ = new clsOntologyItem()
                {
                    GUID = objOList_attribute_part____.First().ID_Other,
                    Name = objOList_attribute_part____.First().Name_Other,
                    GUID_Parent = objOList_attribute_part____.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_percent = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "attribute_percent".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                              select objRef).ToList();

            if (objOList_attribute_percent.Any())
            {
                OItem_attribute_percent = new clsOntologyItem()
                {
                    GUID = objOList_attribute_percent.First().ID_Other,
                    Name = objOList_attribute_percent.First().Name_Other,
                    GUID_Parent = objOList_attribute_percent.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            

            var objOList_attribute_to_pay = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "attribute_to_pay".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                             select objRef).ToList();

            if (objOList_attribute_to_pay.Any())
            {
                OItem_attribute_to_pay = new clsOntologyItem()
                {
                    GUID = objOList_attribute_to_pay.First().ID_Other,
                    Name = objOList_attribute_to_pay.First().Name_Other,
                    GUID_Parent = objOList_attribute_to_pay.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_transaction_date = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "attribute_transaction_date".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                       select objRef).ToList();

            if (objOList_attribute_transaction_date.Any())
            {
                OItem_attribute_transaction_date = new clsOntologyItem()
                {
                    GUID = objOList_attribute_transaction_date.First().ID_Other,
                    Name = objOList_attribute_transaction_date.First().Name_Other,
                    GUID_Parent = objOList_attribute_transaction_date.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_transaction_id = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "attribute_transaction_id".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                     select objRef).ToList();

            if (objOList_attribute_transaction_id.Any())
            {
                OItem_attribute_transaction_id = new clsOntologyItem()
                {
                    GUID = objOList_attribute_transaction_id.First().ID_Other,
                    Name = objOList_attribute_transaction_id.First().Name_Other,
                    GUID_Parent = objOList_attribute_transaction_id.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_valutatag = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_valutatag".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

            if (objOList_attribute_valutatag.Any())
            {
                OItem_attribute_valutatag = new clsOntologyItem()
                {
                    GUID = objOList_attribute_valutatag.First().ID_Other,
                    Name = objOList_attribute_valutatag.First().Name_Other,
                    GUID_Parent = objOList_attribute_valutatag.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_zahlungsausgang = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "attribute_zahlungsausgang".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                      select objRef).ToList();

            if (objOList_attribute_zahlungsausgang.Any())
            {
                OItem_attribute_zahlungsausgang = new clsOntologyItem()
                {
                    GUID = objOList_attribute_zahlungsausgang.First().ID_Other,
                    Name = objOList_attribute_zahlungsausgang.First().Name_Other,
                    GUID_Parent = objOList_attribute_zahlungsausgang.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_RelationTypes()
        {
            var objOList_relationtype_accountings = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "relationtype_accountings".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                     select objRef).ToList();

            if (objOList_relationtype_accountings.Any())
            {
                OItem_relationtype_accountings = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_accountings.First().ID_Other,
                    Name = objOList_relationtype_accountings.First().Name_Other,
                    GUID_Parent = objOList_relationtype_accountings.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_amount = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "relationtype_belonging_amount".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                          select objRef).ToList();

            if (objOList_relationtype_belonging_amount.Any())
            {
                OItem_relationtype_belonging_amount = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_amount.First().ID_Other,
                    Name = objOList_relationtype_belonging_amount.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_amount.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_contractee = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "relationtype_belonging_contractee".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                              select objRef).ToList();

            if (objOList_relationtype_belonging_contractee.Any())
            {
                OItem_relationtype_belonging_contractee = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_contractee.First().ID_Other,
                    Name = objOList_relationtype_belonging_contractee.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_contractee.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_contractor = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "relationtype_belonging_contractor".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                              select objRef).ToList();

            if (objOList_relationtype_belonging_contractor.Any())
            {
                OItem_relationtype_belonging_contractor = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_contractor.First().ID_Other,
                    Name = objOList_relationtype_belonging_contractor.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_contractor.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_currency = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "relationtype_belonging_currency".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                            select objRef).ToList();

            if (objOList_relationtype_belonging_currency.Any())
            {
                OItem_relationtype_belonging_currency = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_currency.First().ID_Other,
                    Name = objOList_relationtype_belonging_currency.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_currency.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_payment = (from objOItem in objDBLevel_Config1.ObjectRels
                                                           where objOItem.ID_Object == cstrID_Ontology
                                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                           where objRef.Name_Object.ToLower() == "relationtype_belonging_payment".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                           select objRef).ToList();

            if (objOList_relationtype_belonging_payment.Any())
            {
                OItem_relationtype_belonging_payment = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_payment.First().ID_Other,
                    Name = objOList_relationtype_belonging_payment.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_payment.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_sem_item = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "relationtype_belonging_sem_item".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                            select objRef).ToList();

            if (objOList_relationtype_belonging_sem_item.Any())
            {
                OItem_relationtype_belonging_sem_item = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_sem_item.First().ID_Other,
                    Name = objOList_relationtype_belonging_sem_item.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_sem_item.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_tax_rate = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "relationtype_belonging_tax_rate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                            select objRef).ToList();

            if (objOList_relationtype_belonging_tax_rate.Any())
            {
                OItem_relationtype_belonging_tax_rate = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_tax_rate.First().ID_Other,
                    Name = objOList_relationtype_belonging_tax_rate.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_tax_rate.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_belongsto.Any())
            {
                OItem_relationtype_belongsto = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belongsto.First().ID_Other,
                    Name = objOList_relationtype_belongsto.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_gegenkonto = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_gegenkonto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_gegenkonto.Any())
            {
                OItem_relationtype_gegenkonto = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_gegenkonto.First().ID_Other,
                    Name = objOList_relationtype_gegenkonto.First().Name_Other,
                    GUID_Parent = objOList_relationtype_gegenkonto.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_of_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_is_of_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_is_of_type.Any())
            {
                OItem_relationtype_is_of_type = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_of_type.First().ID_Other,
                    Name = objOList_relationtype_is_of_type.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_of_type.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_isdescribedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_isdescribedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_isdescribedby.Any())
            {
                OItem_relationtype_isdescribedby = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_isdescribedby.First().ID_Other,
                    Name = objOList_relationtype_isdescribedby.First().Name_Other,
                    GUID_Parent = objOList_relationtype_isdescribedby.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_located_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_located_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_located_in.Any())
            {
                OItem_relationtype_located_in = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_located_in.First().ID_Other,
                    Name = objOList_relationtype_located_in.First().Name_Other,
                    GUID_Parent = objOList_relationtype_located_in.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_offered_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_offered_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_offered_by.Any())
            {
                OItem_relationtype_offered_by = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_offered_by.First().ID_Other,
                    Name = objOList_relationtype_offered_by.First().Name_Other,
                    GUID_Parent = objOList_relationtype_offered_by.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_offers = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_offers".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

            if (objOList_relationtype_offers.Any())
            {
                OItem_relationtype_offers = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_offers.First().ID_Other,
                    Name = objOList_relationtype_offers.First().Name_Other,
                    GUID_Parent = objOList_relationtype_offers.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_standard = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_standard".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_standard.Any())
            {
                OItem_relationtype_standard = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_standard.First().ID_Other,
                    Name = objOList_relationtype_standard.First().Name_Other,
                    GUID_Parent = objOList_relationtype_standard.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_zugeh_rige_mandanten = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "relationtype_zugeh_rige_mandanten".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                              select objRef).ToList();

            if (objOList_relationtype_zugeh_rige_mandanten.Any())
            {
                OItem_relationtype_zugeh_rige_mandanten = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_zugeh_rige_mandanten.First().ID_Other,
                    Name = objOList_relationtype_zugeh_rige_mandanten.First().Name_Other,
                    GUID_Parent = objOList_relationtype_zugeh_rige_mandanten.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Objects()
        {
            var objOList_token_direction_down = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "token_direction_down".ToLower() && objRef.Ontology == Globals.Type_Object
                                                 select objRef).ToList();

            if (objOList_token_direction_down.Any())
            {
                OItem_token_direction_down = new clsOntologyItem()
                {
                    GUID = objOList_token_direction_down.First().ID_Other,
                    Name = objOList_token_direction_down.First().Name_Other,
                    GUID_Parent = objOList_token_direction_down.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_direction_up = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "token_direction_up".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

            if (objOList_token_direction_up.Any())
            {
                OItem_token_direction_up = new clsOntologyItem()
                {
                    GUID = objOList_token_direction_up.First().ID_Other,
                    Name = objOList_token_direction_up.First().Name_Other,
                    GUID_Parent = objOList_token_direction_up.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_module_bill_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_module_bill_module".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

            if (objOList_token_module_bill_module.Any())
            {
                OItem_token_module_bill_module = new clsOntologyItem()
                {
                    GUID = objOList_token_module_bill_module.First().ID_Other,
                    Name = objOList_token_module_bill_module.First().Name_Other,
                    GUID_Parent = objOList_token_module_bill_module.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_search_template_amount_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "token_search_template_amount_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                          select objRef).ToList();

            if (objOList_token_search_template_amount_.Any())
            {
                OItem_token_search_template_amount_ = new clsOntologyItem()
                {
                    GUID = objOList_token_search_template_amount_.First().ID_Other,
                    Name = objOList_token_search_template_amount_.First().Name_Other,
                    GUID_Parent = objOList_token_search_template_amount_.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_search_template_contractee_contractor_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                         where objOItem.ID_Object == cstrID_Ontology
                                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                         where objRef.Name_Object.ToLower() == "token_search_template_contractee_contractor_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                         select objRef).ToList();

            if (objOList_token_search_template_contractee_contractor_.Any())
            {
                OItem_token_search_template_contractee_contractor_ = new clsOntologyItem()
                {
                    GUID = objOList_token_search_template_contractee_contractor_.First().ID_Other,
                    Name = objOList_token_search_template_contractee_contractor_.First().Name_Other,
                    GUID_Parent = objOList_token_search_template_contractee_contractor_.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_search_template_name_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "token_search_template_name_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                        select objRef).ToList();

            if (objOList_token_search_template_name_.Any())
            {
                OItem_token_search_template_name_ = new clsOntologyItem()
                {
                    GUID = objOList_token_search_template_name_.First().ID_Other,
                    Name = objOList_token_search_template_name_.First().Name_Other,
                    GUID_Parent = objOList_token_search_template_name_.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_search_template_payment_date_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                where objOItem.ID_Object == cstrID_Ontology
                                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                where objRef.Name_Object.ToLower() == "token_search_template_payment_date_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                select objRef).ToList();

            if (objOList_token_search_template_payment_date_.Any())
            {
                OItem_token_search_template_payment_date_ = new clsOntologyItem()
                {
                    GUID = objOList_token_search_template_payment_date_.First().ID_Other,
                    Name = objOList_token_search_template_payment_date_.First().Name_Other,
                    GUID_Parent = objOList_token_search_template_payment_date_.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_search_template_related_sem_item_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_search_template_related_sem_item_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

            if (objOList_token_search_template_related_sem_item_.Any())
            {
                OItem_token_search_template_related_sem_item_ = new clsOntologyItem()
                {
                    GUID = objOList_token_search_template_related_sem_item_.First().ID_Other,
                    Name = objOList_token_search_template_related_sem_item_.First().Name_Other,
                    GUID_Parent = objOList_token_search_template_related_sem_item_.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_search_template_to_pay_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "token_search_template_to_pay_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                          select objRef).ToList();

            if (objOList_token_search_template_to_pay_.Any())
            {
                OItem_token_search_template_to_pay_ = new clsOntologyItem()
                {
                    GUID = objOList_token_search_template_to_pay_.First().ID_Other,
                    Name = objOList_token_search_template_to_pay_.First().Name_Other,
                    GUID_Parent = objOList_token_search_template_to_pay_.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_search_template_transaction_date_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_search_template_transaction_date_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

            if (objOList_token_search_template_transaction_date_.Any())
            {
                OItem_token_search_template_transaction_date_ = new clsOntologyItem()
                {
                    GUID = objOList_token_search_template_transaction_date_.First().ID_Other,
                    Name = objOList_token_search_template_transaction_date_.First().Name_Other,
                    GUID_Parent = objOList_token_search_template_transaction_date_.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_software_development_bill_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                   where objOItem.ID_Object == cstrID_Ontology
                                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                   where objRef.Name_Object.ToLower() == "token_software_development_bill_module".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                   select objRef).ToList();

            if (objOList_token_software_development_bill_module.Any())
            {
                OItem_token_software_development_bill_module = new clsOntologyItem()
                {
                    GUID = objOList_token_software_development_bill_module.First().ID_Other,
                    Name = objOList_token_software_development_bill_module.First().Name_Other,
                    GUID_Parent = objOList_token_software_development_bill_module.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Classes()
        {
            var objOList_type_bank_transaktionen__sparkasse_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                where objOItem.ID_Object == cstrID_Ontology
                                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                where objRef.Name_Object.ToLower() == "type_bank_transaktionen__sparkasse_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                                select objRef).ToList();

            if (objOList_type_bank_transaktionen__sparkasse_.Any())
            {
                OItem_type_bank_transaktionen__sparkasse_ = new clsOntologyItem()
                {
                    GUID = objOList_type_bank_transaktionen__sparkasse_.First().ID_Other,
                    Name = objOList_type_bank_transaktionen__sparkasse_.First().Name_Other,
                    GUID_Parent = objOList_type_bank_transaktionen__sparkasse_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_bank_transaktionen__sparkasse____archiv = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                         where objOItem.ID_Object == cstrID_Ontology
                                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                         where objRef.Name_Object.ToLower() == "type_bank_transaktionen__sparkasse____archiv".ToLower() && objRef.Ontology == Globals.Type_Class
                                                                         select objRef).ToList();

            if (objOList_type_bank_transaktionen__sparkasse____archiv.Any())
            {
                OItem_type_bank_transaktionen__sparkasse____archiv = new clsOntologyItem()
                {
                    GUID = objOList_type_bank_transaktionen__sparkasse____archiv.First().ID_Other,
                    Name = objOList_type_bank_transaktionen__sparkasse____archiv.First().Name_Other,
                    GUID_Parent = objOList_type_bank_transaktionen__sparkasse____archiv.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_bankleitzahl = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_bankleitzahl".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_type_bankleitzahl.Any())
            {
                OItem_type_bankleitzahl = new clsOntologyItem()
                {
                    GUID = objOList_type_bankleitzahl.First().ID_Other,
                    Name = objOList_type_bankleitzahl.First().Name_Other,
                    GUID_Parent = objOList_type_bankleitzahl.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_beleg = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_beleg".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_type_beleg.Any())
            {
                OItem_type_beleg = new clsOntologyItem()
                {
                    GUID = objOList_type_beleg.First().ID_Other,
                    Name = objOList_type_beleg.First().Name_Other,
                    GUID_Parent = objOList_type_beleg.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_belegsart = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_belegsart".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_type_belegsart.Any())
            {
                OItem_type_belegsart = new clsOntologyItem()
                {
                    GUID = objOList_type_belegsart.First().ID_Other,
                    Name = objOList_type_belegsart.First().Name_Other,
                    GUID_Parent = objOList_type_belegsart.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_bill_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_bill_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_type_bill_module.Any())
            {
                OItem_type_bill_module = new clsOntologyItem()
                {
                    GUID = objOList_type_bill_module.First().ID_Other,
                    Name = objOList_type_bill_module.First().Name_Other,
                    GUID_Parent = objOList_type_bill_module.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_container__physical_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "type_container__physical_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                      select objRef).ToList();

            if (objOList_type_container__physical_.Any())
            {
                OItem_type_container__physical_ = new clsOntologyItem()
                {
                    GUID = objOList_type_container__physical_.First().ID_Other,
                    Name = objOList_type_container__physical_.First().Name_Other,
                    GUID_Parent = objOList_type_container__physical_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_currencies = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_currencies".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

            if (objOList_type_currencies.Any())
            {
                OItem_type_currencies = new clsOntologyItem()
                {
                    GUID = objOList_type_currencies.First().ID_Other,
                    Name = objOList_type_currencies.First().Name_Other,
                    GUID_Parent = objOList_type_currencies.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_einheit = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_einheit".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_type_einheit.Any())
            {
                OItem_type_einheit = new clsOntologyItem()
                {
                    GUID = objOList_type_einheit.First().ID_Other,
                    Name = objOList_type_einheit.First().Name_Other,
                    GUID_Parent = objOList_type_einheit.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_financial_transaction = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "type_financial_transaction".ToLower() && objRef.Ontology == Globals.Type_Class
                                                       select objRef).ToList();

            if (objOList_type_financial_transaction.Any())
            {
                OItem_type_financial_transaction = new clsOntologyItem()
                {
                    GUID = objOList_type_financial_transaction.First().ID_Other,
                    Name = objOList_type_financial_transaction.First().Name_Other,
                    GUID_Parent = objOList_type_financial_transaction.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_financial_transaction___archive = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                 where objRef.Name_Object.ToLower() == "type_financial_transaction___archive".ToLower() && objRef.Ontology == Globals.Type_Class
                                                                 select objRef).ToList();

            if (objOList_type_financial_transaction___archive.Any())
            {
                OItem_type_financial_transaction___archive = new clsOntologyItem()
                {
                    GUID = objOList_type_financial_transaction___archive.First().ID_Other,
                    Name = objOList_type_financial_transaction___archive.First().Name_Other,
                    GUID_Parent = objOList_type_financial_transaction___archive.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_kontonummer = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_kontonummer".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_type_kontonummer.Any())
            {
                OItem_type_kontonummer = new clsOntologyItem()
                {
                    GUID = objOList_type_kontonummer.First().ID_Other,
                    Name = objOList_type_kontonummer.First().Name_Other,
                    GUID_Parent = objOList_type_kontonummer.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_language = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_language".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

            if (objOList_type_language.Any())
            {
                OItem_type_language = new clsOntologyItem()
                {
                    GUID = objOList_type_language.First().ID_Other,
                    Name = objOList_type_language.First().Name_Other,
                    GUID_Parent = objOList_type_language.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_menge = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_menge".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_type_menge.Any())
            {
                OItem_type_menge = new clsOntologyItem()
                {
                    GUID = objOList_type_menge.First().ID_Other,
                    Name = objOList_type_menge.First().Name_Other,
                    GUID_Parent = objOList_type_menge.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_type_module.Any())
            {
                OItem_type_module = new clsOntologyItem()
                {
                    GUID = objOList_type_module.First().ID_Other,
                    Name = objOList_type_module.First().Name_Other,
                    GUID_Parent = objOList_type_module.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_offset = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_offset".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_type_offset.Any())
            {
                OItem_type_offset = new clsOntologyItem()
                {
                    GUID = objOList_type_offset.First().ID_Other,
                    Name = objOList_type_offset.First().Name_Other,
                    GUID_Parent = objOList_type_offset.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_partner = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_partner".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_type_partner.Any())
            {
                OItem_type_partner = new clsOntologyItem()
                {
                    GUID = objOList_type_partner.First().ID_Other,
                    Name = objOList_type_partner.First().Name_Other,
                    GUID_Parent = objOList_type_partner.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_payment = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_payment".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_type_payment.Any())
            {
                OItem_type_payment = new clsOntologyItem()
                {
                    GUID = objOList_type_payment.First().ID_Other,
                    Name = objOList_type_payment.First().Name_Other,
                    GUID_Parent = objOList_type_payment.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_search_template = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "type_search_template".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

            if (objOList_type_search_template.Any())
            {
                OItem_type_search_template = new clsOntologyItem()
                {
                    GUID = objOList_type_search_template.First().ID_Other,
                    Name = objOList_type_search_template.First().Name_Other,
                    GUID_Parent = objOList_type_search_template.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_tax_rates = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_tax_rates".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_type_tax_rates.Any())
            {
                OItem_type_tax_rates = new clsOntologyItem()
                {
                    GUID = objOList_type_tax_rates.First().ID_Other,
                    Name = objOList_type_tax_rates.First().Name_Other,
                    GUID_Parent = objOList_type_tax_rates.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }
    }
}
