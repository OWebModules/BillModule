﻿using BillModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Services
{
    public class ElasticServiceAgentTransaction : ElasticBaseAgent
    {
        private Globals globals;

        public async Task<ResultItem<BaseConfig>> GetBaseConfig()
        {
            var taskResult = await Task.Run<ResultItem<BaseConfig>>(() =>
            {
                var result = new ResultItem<BaseConfig>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new BaseConfig()
                };

                var searchBaseConfig = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = Config.LocalData.Object_Bill_Module1.GUID,
                        ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                        ID_Parent_Object = Config.LocalData.Class_Bill_Module.GUID
                    }
                };

                var dbReader1 = new OntologyModDBConnector(globals);

                result.ResultState = dbReader1.GetDataObjectRel(searchBaseConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the BaseConfig!";
                    return result;
                }

                result.Result.BaseConfigItem = dbReader1.ObjectRels.OrderBy(rel => rel.OrderID).Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.BaseConfigItem == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Baseconfig found!";
                    return result;
                }


                var searchGross = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.BaseConfigItem.GUID,
                        ID_AttributeType = Config.LocalData.AttributeType_Gross__Standard_.GUID
                    }
                };

                var dbReaderGross = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderGross.GetDataObjectAtt(searchGross);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the standard-gross!";
                    return result;
                }

                result.Result.BaseConfigToGrossStandard = dbReaderGross.ObjAtts.FirstOrDefault();

                if (result.Result.BaseConfigToGrossStandard == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No standard-gross found!";
                    return result;
                }

                var searchCurrency = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfigItem.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Bill_Module_Standard_Currencies.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Bill_Module_Standard_Currencies.ID_Class_Right
                    }
                };

                var dbReaderStandardCurrency = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderStandardCurrency.GetDataObjectRel(searchCurrency);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the standard-currency!";
                    return result;
                }

                result.Result.BaseConfigToCurrency = dbReaderStandardCurrency.ObjectRels.FirstOrDefault();

                if (result.Result.BaseConfigToCurrency == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No base-currency found!";
                    return result;
                }

                var searchUnit = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfigItem.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Bill_Module_Standard_Einheit.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Bill_Module_Standard_Einheit.ID_Class_Right
                    }
                };

                var dbReaderStandardUnit = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderStandardUnit.GetDataObjectRel(searchUnit);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the standard-unit!";
                    return result;
                }

                result.Result.BaseConfigToUnit = dbReaderStandardUnit.ObjectRels.FirstOrDefault();

                if (result.Result.BaseConfigToUnit == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No standard-unit found!";
                    return result;
                }

                var searchMandanten = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfigItem.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Bill_Module_zugeh_rige_Mandanten_Partner.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Bill_Module_zugeh_rige_Mandanten_Partner.ID_Class_Right
                    }
                };

                var dbReaderMandanten = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMandanten.GetDataObjectRel(searchMandanten);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the tenant!";
                    return result;

                }

                result.Result.BaseConfigToMandanten = dbReaderMandanten.ObjectRels;

                return result;
            });

            return taskResult;
        }


        public async Task<clsOntologyItem> SaveTransactionDate(clsOntologyItem transactionItem, DateTime? transactionDate)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                if (transactionDate != null)
                {
                    var relTransactionDate = relationConfig.Rel_ObjectAttribute(transactionItem, 
                        Config.LocalData.AttributeType_Transaction_Date,
                        transactionDate.Value);
                    result = transaction.do_Transaction(relTransactionDate, true);
                }
                else
                {
                    var rel = new clsObjectAtt
                    {
                        ID_Object = transactionItem.GUID,
                        ID_AttributeType = Config.LocalData.ClassAtt_Financial_Transaction_Transaction_Date.ID_AttributeType
                    };

                    var dbWriter = new OntologyModDBConnector(globals);
                    result = dbWriter.DelObjectAtts(new List<clsObjectAtt> { rel });
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveTransactionId(clsOntologyItem transactionItem, string transactionId)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var relTransactionId = relationConfig.Rel_ObjectAttribute(transactionItem, 
                    Config.LocalData.AttributeType_Transaction_ID,
                    transactionId);
                result = transaction.do_Transaction(relTransactionId, true);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TransactionAmount>> SaveTransactionAmount(clsOntologyItem transactionItem, double amount, string idUnit)
        {
            var taskResult = await Task.Run<ResultItem<TransactionAmount>>(async () =>
            {
                var result = new ResultItem<TransactionAmount>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TransactionAmount()
                };

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var searchUnitResult = await GetClassObject(idUnit);

                result.Result.Unit = searchUnitResult.Result.ObjectItem;
                var searchMenge = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = transactionItem.GUID,
                        ID_AttributeType = Config.LocalData.ClassAtt_Menge_Menge.ID_AttributeType,
                        Val_Double = amount
                    }
                };

                var dbReaderMenge = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMenge.GetDataObjectAtt(searchMenge);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchMengeToUnit = dbReaderMenge.ObjAtts.Select(att => new clsObjectRel
                {
                    ID_Object = att.ID_Object,
                    ID_Other = idUnit
                }).ToList();

                var dbReaderMengeUnit = new OntologyModDBConnector(globals);

                if (searchMengeToUnit.Any())
                {
                    result.ResultState = dbReaderMengeUnit.GetDataObjectRel(searchMengeToUnit);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                

                clsOntologyItem mengeItm = null;
                if (dbReaderMengeUnit.ObjectRels.Any())
                {
                    var mengeUnitRel = dbReaderMengeUnit.ObjectRels.First();

                    mengeItm = new clsOntologyItem
                    {
                        GUID = mengeUnitRel.ID_Object,
                        Name = mengeUnitRel.Name_Object,
                        GUID_Parent = Config.LocalData.Class_Menge.GUID,
                        Type = globals.Type_Object
                    };

                    result.Result.AmountAmount = (from att in dbReaderMengeUnit.ObjAtts
                                                  where att.ID_Object == mengeItm.GUID
                                                  select att).First();
                }

                if (mengeItm == null)
                {
                    mengeItm = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = $"{amount.ToString()} {searchUnitResult.Result.ObjectItem.Name}",
                        GUID_Parent = Config.LocalData.Class_Menge.GUID,
                        Type = globals.Type_Object
                    };

                    result.ResultState = transaction.do_Transaction(mengeItm);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var relAmount = relationConfig.Rel_ObjectAttribute(mengeItm, 
                        Config.LocalData.AttributeType_Menge,
                        amount);

                    result.ResultState = transaction.do_Transaction(relAmount);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    result.Result.AmountAmount = transaction.OItem_Last.OItemObjectAtt;

                    var relUnit = relationConfig.Rel_ObjectRelation(mengeItm, 
                        searchUnitResult.Result.ObjectItem, 
                        Config.LocalData.RelationType_is_of_Type);

                    result.ResultState = transaction.do_Transaction(relUnit);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    
                }

                var relMenge = relationConfig.Rel_ObjectRelation(transactionItem, mengeItm, 
                    Config.LocalData.RelationType_belonging_Amount);

                transaction.ClearItems();
                result.ResultState = transaction.do_Transaction(relMenge, true);

                result.Result.Amount = mengeItm;
                
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> SaveTransactionName(string idTransactionItem, string name)
        {
            var taskResult = await Task.Run< ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbWriter = new OntologyModDBConnector(globals);

                var save = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idTransactionItem,
                        Name = name,
                        GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                        Type = globals.Type_Object
                    }
                };

                if (string.IsNullOrEmpty(idTransactionItem) )
                {
                    save.First().GUID = globals.NewGUID;
                }

                var resultSave = dbWriter.SaveObjects(save);

                if (resultSave.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultSave;
                    return result;
                }

                result.Result = save.First();
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> SaveTaxRate(clsOntologyItem transactionItem, string idTaxRate)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbConnector = new OntologyModDBConnector(globals);

                var searchTaxRate = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idTaxRate,
                        GUID_Parent = Config.LocalData.Class_Tax_Rates.GUID
                    }
                };

                result.ResultState = dbConnector.GetDataObjects(searchTaxRate);

                if (result.ResultState.GUID == globals.LState_Error.GUID || !dbConnector.Objects1.Any())
                {
                    return result;
                }

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var relTransactionToTaxRate = relationConfig.Rel_ObjectRelation(transactionItem, 
                    dbConnector.Objects1.First(), 
                    Config.LocalData.RelationType_belonging_Tax_Rate);
                result.ResultState = transaction.do_Transaction(relTransactionToTaxRate, true);
                result.Result = dbConnector.Objects1.First();
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> SaveCurrency(clsOntologyItem transactionItem, string idCurrency)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbConnector = new OntologyModDBConnector(globals);

                var searchCurrency = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idCurrency,
                        GUID_Parent = Config.LocalData.Class_Currencies.GUID
                    }
                };

                result.ResultState = dbConnector.GetDataObjects(searchCurrency);

                if (result.ResultState.GUID == globals.LState_Error.GUID || !dbConnector.Objects1.Any())
                {
                    return result;
                }

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var relTransactionToCurrency = relationConfig.Rel_ObjectRelation(transactionItem, 
                    dbConnector.Objects1.First(), 
                    Config.LocalData.RelationType_belonging_Currency);
                result.ResultState = transaction.do_Transaction(relTransactionToCurrency, true);
                result.Result = dbConnector.Objects1.First();
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveToPay(clsOntologyItem transactionItem, double toPay)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var relTransactionToPay = relationConfig.Rel_ObjectAttribute(transactionItem, 
                    Config.LocalData.AttributeType_to_Pay, 
                    toPay);
                result = transaction.do_Transaction(relTransactionToPay, true);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsObjectAtt>> SaveGross(clsOntologyItem transactionItem, bool gross)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectAtt>>(() =>
            {
                var result = new ResultItem<clsObjectAtt>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var relTransactionGross = relationConfig.Rel_ObjectAttribute(transactionItem, 
                    Config.LocalData.AttributeType_gross,
                    gross);
                result.ResultState = transaction.do_Transaction(relTransactionGross, true);
                result.Result = transaction.OItem_Last.OItemObjectAtt;
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveContractor(clsOntologyItem transactionItem, clsOntologyItem partner)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var relTransactionContractor = relationConfig.Rel_ObjectRelation(transactionItem, 
                    partner, 
                    Config.LocalData.RelationType_belonging_Contractor);
                result = transaction.do_Transaction(relTransactionContractor, true);
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveContractee(clsOntologyItem transactionItem, clsOntologyItem partner)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                var relTransactionContractor = relationConfig.Rel_ObjectRelation(transactionItem, 
                    partner, 
                    Config.LocalData.RelationType_belonging_Contractee);
                result = transaction.do_Transaction(relTransactionContractor, true);
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> SaveObjects(List<clsOntologyItem> objectsToSave)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
            var result = new ResultItem<List<clsOntologyItem>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = objectsToSave
            };

                objectsToSave.Where(obj => string.IsNullOrEmpty(obj.GUID)).ToList().ForEach(obj => obj.GUID = globals.NewGUID);

                var dbConnectorObjects = new OntologyModDBConnector(globals);

                result.ResultState = dbConnectorObjects.SaveObjects(objectsToSave);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the transactions!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckOrCreateObjectsByName(List<clsOntologyItem> objectsToCheck)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = objectsToCheck
                };

                var dbConnectorObjects = new OntologyModDBConnector(globals);

                result.ResultState = dbConnectorObjects.GetDataObjects(objectsToCheck);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the objects!";
                    return result;
                }

                (from obj in objectsToCheck
                 join objExist in dbConnectorObjects.Objects1 on obj.Name equals objExist.Name into objExists
                 from objExist in objExists.DefaultIfEmpty()
                 select new { obj, objExist }).ToList().ForEach(obj =>
                  {
                      if (obj.objExist != null)
                      {
                          obj.obj.GUID = obj.objExist.GUID;
                          obj.obj.New_Item = false;
                      }
                      else
                      {
                          obj.obj.GUID = globals.NewGUID;
                          obj.obj.New_Item = true;
                      }
                  });

                var objectsToSave = objectsToCheck.Where(obj => obj.New_Item.Value).ToList();

                if (objectsToSave.Any())
                {
                    result.ResultState = dbConnectorObjects.SaveObjects(objectsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the objects!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TransactionItemsRaw>> GetTransactionItems(clsOntologyItem refItem, bool detailView = true)
        {
            var taskResult = await Task.Run<ResultItem<TransactionItemsRaw>>(() =>
            {
                var result = new ResultItem<TransactionItemsRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TransactionItemsRaw()
                    {
                        RefItem = refItem
                    }
                };

                if (refItem.GUID_Parent == Config.LocalData.Class_Partner.GUID)
                {
                    result.Result.Mandant = refItem;
                    var searchPartnerToTransactionItemsLeftRight = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = result.Result.Mandant.GUID,
                            ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_RelationType,
                            ID_Parent_Object = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_Class_Left
                        }
                    };

                    var dbReaderPartnerToTransactionItemsLeftRight = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderPartnerToTransactionItemsLeftRight.GetDataObjectRel(searchPartnerToTransactionItemsLeftRight);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionItems = dbReaderPartnerToTransactionItemsLeftRight.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Object,
                        Name = objRel.Name_Object,
                        GUID_Parent = objRel.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).ToList();

                    var searchPartnerToTransactionItemsRightLeft = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = result.Result.Mandant.GUID,
                            ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractor_Partner.ID_RelationType,
                            ID_Parent_Object = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractor_Partner.ID_Class_Left
                        }
                    };

                    var dbReaderPartnerToTransactionItemsRightLeft = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderPartnerToTransactionItemsRightLeft.GetDataObjectRel(searchPartnerToTransactionItemsRightLeft);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionItems.AddRange(dbReaderPartnerToTransactionItemsRightLeft.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Object,
                        Name = objRel.Name_Object,
                        GUID_Parent = objRel.ID_Parent_Object,
                        Type = globals.Type_Object
                    }));
                }
                else if (detailView)
                {
                    result.Result.TransactionItems = new List<clsOntologyItem>
                    {
                        refItem
                    };
                }
                else
                {
                    var searchTransactions = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = refItem.GUID,
                            ID_RelationType = Config.LocalData.RelationType_contains.GUID,
                            ID_Parent_Other = Config.LocalData.Class_Financial_Transaction.GUID
                        }
                    };

                    var dbReaderTransactions = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderTransactions.GetDataObjectRel(searchTransactions);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionItems = dbReaderTransactions.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();
                }
                

                var searchSubTransactions = result.Result.TransactionItems.Select(trans => new clsObjectRel
                {
                    ID_Other = trans.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_contains_Financial_Transaction.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Financial_Transaction_contains_Financial_Transaction.ID_Class_Left
                }).ToList();

                var dbReaderSubTransactions = new OntologyModDBConnector(globals);

                if (searchSubTransactions.Any())
                {
                    result.ResultState = dbReaderSubTransactions.GetDataObjectRel(searchSubTransactions);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.SubTransactions = (from trans in result.Result.TransactionItems
                                                                   join subTranRel in dbReaderSubTransactions.ObjectRels on trans.GUID equals subTranRel.ID_Object
                                                                   join subTran in result.Result.TransactionItems on subTranRel.ID_Other equals subTran.GUID into subTrans
                                                                   from subTran in subTrans.DefaultIfEmpty()
                                                                   where subTran != null
                                                                   select subTran).ToList();
                }

                result.Result.TransactionItems = (from trans in result.Result.TransactionItems
                                                  join subTran in result.Result.SubTransactions on trans.GUID equals subTran.GUID into subTrans
                                                  from subTran in subTrans.DefaultIfEmpty()
                                                  where subTran == null
                                                  select trans).ToList();

                var searchTransactionDates = result.Result.TransactionItems.Select(trans => new clsObjectAtt
                {
                    ID_Object = trans.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Financial_Transaction_Transaction_Date.ID_AttributeType
                }).ToList();

                var dbReaderTransactionDates = new OntologyModDBConnector(globals);

                if (searchTransactionDates.Any())
                {
                    result.ResultState = dbReaderTransactionDates.GetDataObjectAtt(searchTransactionDates);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionDates = dbReaderTransactionDates.ObjAtts;
                }

                var searchGross = result.Result.TransactionItems.Select(trans => new clsObjectAtt
                {
                    ID_Object = trans.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Financial_Transaction_gross.ID_AttributeType
                }).ToList();

                var dbReaderGross = new OntologyModDBConnector(globals);

                if (searchGross.Any())
                {
                    result.ResultState = dbReaderGross.GetDataObjectAtt(searchGross);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.Gross = dbReaderGross.ObjAtts;
                }

                var searchToPay = result.Result.TransactionItems.Select(trans => new clsObjectAtt
                {
                    ID_Object = trans.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Financial_Transaction_to_Pay.ID_AttributeType
                }).ToList();

                var dbReaderToPay = new OntologyModDBConnector(globals);

                if (searchToPay.Any())
                {
                    result.ResultState = dbReaderToPay.GetDataObjectAtt(searchToPay);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.ToPay = dbReaderToPay.ObjAtts;
                }

                var searchId = result.Result.TransactionItems.Select(trans => new clsObjectAtt
                {
                    ID_Object = trans.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Financial_Transaction_Transaction_ID.ID_AttributeType
                }).ToList();

                var dbReaderId = new OntologyModDBConnector(globals);

                if (searchId.Any())
                {
                    result.ResultState = dbReaderId.GetDataObjectAtt(searchId);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionIds = dbReaderId.ObjAtts;
                }

                var searchCurrencies = result.Result.TransactionItems.Select(trans => new clsObjectRel
                {
                    ID_Object = trans.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Currency_Currencies.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Currency_Currencies.ID_Class_Right
                }).ToList();

                var dbReaderCurrencies = new OntologyModDBConnector(globals);

                if (searchCurrencies.Any())
                {
                    result.ResultState = dbReaderCurrencies.GetDataObjectRel(searchCurrencies);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionsToCurrencies = dbReaderCurrencies.ObjectRels;
                }


                var searchMenge = result.Result.TransactionItems.Select(trans => new clsObjectRel
                {
                    ID_Object = trans.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Amount_Menge.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Amount_Menge.ID_Class_Right
                }).ToList();

                var dbReaderMenge = new OntologyModDBConnector(globals);

                if (searchMenge.Any())
                {
                    result.ResultState = dbReaderMenge.GetDataObjectRel(searchMenge);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionsToAmounts = dbReaderMenge.ObjectRels;
                }

                var searchAmount = result.Result.TransactionsToAmounts.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Menge_Menge.ID_AttributeType
                }).ToList();

                var dbReaderAmount = new OntologyModDBConnector(globals);

                if (searchAmount.Any())
                {
                    result.ResultState = dbReaderAmount.GetDataObjectAtt(searchAmount);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.AmountsToAmounts = dbReaderAmount.ObjAtts;
                }

                var searchUnits = result.Result.TransactionsToAmounts.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Menge_is_of_Type_Einheit.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Menge_is_of_Type_Einheit.ID_Class_Right
                }).ToList();

                var dbReaderAmountUnit = new OntologyModDBConnector(globals);

                if (searchUnits.Any())
                {
                    result.ResultState = dbReaderAmountUnit.GetDataObjectRel(searchUnits);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.AmountsToUnits = dbReaderAmountUnit.ObjectRels;
                }


                var searchContractors = result.Result.TransactionItems.Select(trans => new clsObjectRel
                {
                    ID_Object = trans.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractor_Partner.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractor_Partner.ID_Class_Right
                }).ToList();

                var dbReaderContractors = new OntologyModDBConnector(globals);

                if (searchContractors.Any())
                {
                    result.ResultState = dbReaderContractors.GetDataObjectRel(searchContractors);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionContractors = dbReaderContractors.ObjectRels;
                }

                var searchContractees = result.Result.TransactionItems.Select(trans => new clsObjectRel
                {
                    ID_Object = trans.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_Class_Right
                }).ToList();

                var dbReaderContractees = new OntologyModDBConnector(globals);

                if (searchContractees.Any())
                {
                    result.ResultState = dbReaderContractees.GetDataObjectRel(searchContractees);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionContractees = dbReaderContractees.ObjectRels;
                }

                var searchTaxRages = result.Result.TransactionItems.Select(trans => new clsObjectRel
                {
                    ID_Object = trans.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Tax_Rate_Tax_Rates.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Tax_Rate_Tax_Rates.ID_Class_Right
                }).ToList();

                var dbReaderTaxRates = new OntologyModDBConnector(globals);

                if (searchTaxRages.Any())
                {
                    result.ResultState = dbReaderTaxRates.GetDataObjectRel(searchTaxRages);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.TransactionTaxRates = dbReaderTaxRates.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetTransactionHierarhyResult>> GetTransactionsToSync(SyncFinancialTransactionsParentToChildrenRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<GetTransactionHierarhyResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetTransactionHierarhyResult()
                };

                var searchTransactions = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.ParentId,
                        GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID
                    }
                };

                var dbReaderTransactions = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTransactions.GetDataObjects(searchTransactions);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the financial Transactions!";
                    return result;
                }

                result.Result.FinancialTransactions = dbReaderTransactions.Objects1;

                var searchChildren = result.Result.FinancialTransactions.Select(ft => new clsObjectRel
                {
                    ID_Object = ft.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_contains_Financial_Transaction.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_contains_Financial_Transaction.ID_Class_Right
                }).ToList();

                var dbReaderHierarchy = new OntologyModDBConnector(globals);

                if (searchChildren.Any())
                {
                    result.ResultState = dbReaderHierarchy.GetDataObjectRel(searchChildren);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relations between financial Transactions!";
                        return result;
                    }
                }
                result.Result.FinancialTransactionsHierarchy = dbReaderHierarchy.ObjectRels;

                var childItemsToAdd = (from childItem in result.Result.FinancialTransactionsHierarchy
                                       join itemExist in result.Result.FinancialTransactions on childItem.ID_Other equals itemExist.GUID into itemsExist
                                       from itemExist in itemsExist.DefaultIfEmpty()
                                       where itemExist == null
                                       select new clsOntologyItem
                                       {
                                           GUID = childItem.ID_Other,
                                           Name = childItem.Name_Other,
                                           GUID_Parent = childItem.ID_Parent_Other,
                                           Type = childItem.Ontology
                                       }).ToList();
                result.Result.FinancialTransactions.AddRange(childItemsToAdd);

                var searchAttributes = result.Result.FinancialTransactions.Select(ft => new clsObjectAtt
                {
                    ID_Object = ft.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_gross.GUID
                }).ToList();

                searchAttributes.AddRange(result.Result.FinancialTransactions.Select(ft => new clsObjectAtt
                {
                    ID_Object = ft.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Transaction_Date.GUID
                }));

                var dbReaderAttributes = new OntologyModDBConnector(globals);

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the attributes of financial Transactions!";
                        return result;
                    }
                }

                result.Result.FinancialTransactionsAttributes = dbReaderAttributes.ObjAtts;

                var searchRelations = result.Result.FinancialTransactions.Select(ft => new clsObjectRel
                {
                    ID_Object = ft.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Currency_Currencies.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Currency_Currencies.ID_Class_Right
                }).ToList();

                searchRelations.AddRange(result.Result.FinancialTransactions.Select(ft => new clsObjectRel
                {
                    ID_Object = ft.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_Class_Right
                }));

                searchRelations.AddRange(result.Result.FinancialTransactions.Select(ft => new clsObjectRel
                {
                    ID_Object = ft.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractor_Partner.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractor_Partner.ID_Class_Right
                }));

                var dbReaderRelations = new OntologyModDBConnector(globals);

                if (searchRelations.Any())
                {
                    result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelations);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relations of financial Transactions!";
                        return result;
                    }
                }

                result.Result.FinancialTransactionsRelations = dbReaderRelations.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public ElasticServiceAgentTransaction(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }

    public class TransactionItemsRaw
    {
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem Mandant { get; set; }

        public List<clsOntologyItem> TransactionItems { get; set; }
        public List<clsOntologyItem> SubTransactions { get; set; }
        public List<clsObjectAtt> Gross { get; set; }
        public List<clsObjectAtt> ToPay { get; set; }
        public List<clsObjectAtt> TransactionDates { get; set; }
        public List<clsObjectAtt> TransactionIds { get; set; }

        public List<clsObjectRel> TransactionsToCurrencies { get; set; }
        public List<clsObjectRel> TransactionsToAmounts { get; set; }
        public List<clsObjectAtt> AmountsToAmounts { get; set; }
        public List<clsObjectRel> AmountsToUnits { get; set; }
        public List<clsObjectRel> TransactionContractors { get; set; }
        public List<clsObjectRel> TransactionContractees { get; set; }
        public List<clsObjectRel> TransactionPayments { get; set; }
        public List<clsObjectRel> TransactionTaxRates { get; set; }

        public TransactionItemsRaw()
        {
            TransactionItems = new List<clsOntologyItem>();
            SubTransactions = new List<clsOntologyItem>();
            Gross = new List<clsObjectAtt>();
            ToPay = new List<clsObjectAtt>();
            TransactionDates = new List<clsObjectAtt>();
            TransactionIds = new List<clsObjectAtt>();
            TransactionsToCurrencies = new List<clsObjectRel>();
            TransactionsToAmounts = new List<clsObjectRel>();
            AmountsToAmounts = new List<clsObjectAtt>();
            AmountsToUnits = new List<clsObjectRel>();
            TransactionContractees = new List<clsObjectRel>();
            TransactionContractors = new List<clsObjectRel>();
            TransactionPayments = new List<clsObjectRel>();
            TransactionTaxRates = new List<clsObjectRel>();
        }

        
    }

    public class TransactionAmount
    {
        public clsOntologyItem Amount { get; set; }
        public clsObjectAtt AmountAmount { get; set; }
        public clsOntologyItem Unit { get; set; }
    }

   
}
