﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class SyncBanktransactionsRequest
    {
        public string IdTextParser { get; set; }
        public string Query { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public SyncBanktransactionsRequest(string idTextParser)
        {
            IdTextParser = idTextParser;
        }
    }
}
