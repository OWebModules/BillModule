﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class BaseConfig
    {
        public clsOntologyItem BaseConfigItem { get; set; }
        public clsObjectAtt BaseConfigToGrossStandard { get; set; }
        public clsObjectRel BaseConfigToCurrency { get; set; }
        public clsObjectRel BaseConfigToUnit { get; set; }
        public List<clsObjectRel> BaseConfigToMandanten { get; set; }
        public clsObjectRel TaxRate { get; set; }
    }
}
