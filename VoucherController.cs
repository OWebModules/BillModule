﻿using BillModule.Models;
using BillModule.VoucherParser;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule
{
    public class VoucherController : AppController
    {
        public List<IVoucherParser> GetVoucherParserList()
        {
            return new List<IVoucherParser>
                {
                    new ReweParser(),
                    new CSVParser()
                };
        }

        public async Task ParsePDFVoucher(ParsePDFVoucherRequest request)
        {
            await Task.Run(() =>
            {

            });
        }

        public VoucherController(Globals globals) : base(globals)
        {

        }
    }
}
