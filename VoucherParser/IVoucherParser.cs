﻿using BillModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BillModule.VoucherParser
{
    public interface IVoucherParser
    {
        bool IsResponsible(string text);

        string IdParser { get; }
        string NameParser { get; }

        Task<ResultItem<Voucher>> ParseVoucher(string idReference, string text, Globals globals);

        Task<ResultItem<Voucher>> ParseVoucher(string text, Globals globals);

    }
}
