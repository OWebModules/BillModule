﻿using BillModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BillModule.VoucherParser
{
    public class CSVParser : IVoucherParser
    {
        public string IdParser { get; private set; }

        public string NameParser => "CSV";

        public bool IsResponsible(string text)
        {
            var regexRewe = new Regex(";");
            return regexRewe.Match(text).Success;
        }

        public async Task<ResultItem<Voucher>> ParseVoucher(string text, Globals globals)
        {
            return await ParseVoucher(null, text, globals);
        }
        public async Task<ResultItem<Voucher>> ParseVoucher(string idReference, string text, Globals globals)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<Voucher>
                {
                    ResultState = globals.LState_Success,
                    Result = new Voucher { IdReference = idReference }
                };

                var lines = text.Split('\r', '\n').ToList<string>();

                var sbError = new StringBuilder();
                var success = true;

                var lineNo = 1;
                var lineIx = lineNo - 1;

                var positions = new List<VoucherPosition>();
                while (lineIx < lines.Count)
                {
                    var line = lines[lineIx ];

                    var splittedLine = line.Split(';');
                    var length = splittedLine.Count();
                    if (length != 5)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Line {lineNo} has only {length} entries, but 5 are needed!";
                        break;
                    }
                    var datePre = splittedLine[0];
                    DateTime dateOut;
                    if (!DateTime.TryParse(datePre, out dateOut))
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Line {lineNo}, column 1 for Date is invalid: {datePre}!";
                        break;
                    }

                    result.Result.DateStamp = dateOut;

                    var quantityPre = splittedLine[1];
                    long quantityOut;
                    if (!long.TryParse(quantityPre, out quantityOut))
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Line {lineNo}, column 2 for Quantity is invalid: {quantityPre}!";
                        break;
                    }

                    var unit = splittedLine[2];

                    if (string.IsNullOrEmpty(unit))
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Line {lineNo}, column 3 for Unit is empty!";
                        break;
                    }

                    var description = splittedLine[3];
                    if (string.IsNullOrEmpty(description))
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Line {lineNo}, column 4 for Description is empty!";
                        break;
                    }

                    var amountPre = splittedLine[4];
                    double amountOut;
                    if (!double.TryParse(amountPre, out amountOut))
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Line {lineNo}, column 5 for Amount is invalid: {amountPre}";
                        break;
                    }

                    result.Result.Positions.Add(new VoucherPosition
                    {
                        Quantity = quantityOut,
                        Position = description,
                        Amount = amountOut,
                        Unit = unit
                    });

                    lineNo++;
                    lineIx = lineNo - 1;
                }

                return result;
            });

            return taskResult;
        }

        public CSVParser()
        {
            IdParser = Guid.NewGuid().ToString();
        }
    }
}
