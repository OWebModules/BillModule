﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class ParsePDFVoucherRequest
    {
        public string IdBeleg { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public ParsePDFVoucherRequest(string idBeleg)
        {
            IdBeleg = idBeleg;
        }
    }
}
