﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.True,
        height = "100%")]
    [KendoFilter(mode = "row")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoNumberFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoDateFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class VoucherGridViewItem
    {
        [KendoColumn(hidden = true)]
        public string IdVoucher { get; set; }

        [KendoColumn(hidden = true)]
        public string NameVoucher { get; set; }
        
        [KendoColumn(hidden = true)]
        public string IdSourcePartner { get; set; }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Date")]
        public DateTime Timestamp { get; set; }


        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Source")]
        public string NameSourcePartner { get; set; }


        [KendoColumn(hidden = true)]
        public string IdQuantity { get; set; }

        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Quantity")]
        public double Quantity { get; set; }

        [KendoColumn(hidden = true)]
        public string IdUnit { get; set; }

        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Unit")]
        public string NameUnit { get; set; }

        [KendoColumn(hidden = true)]
        public string IdPosition { get; set; }

        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Description")]
        public string NamePosition { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeAmount { get; set; }

        [KendoColumn(hidden = false, Order = 6, filterable = true, title = "Amount")]
        public double Amount { get; set; }

        [KendoColumn(hidden = true)]
        public string IdCategory { get; set; }

        [KendoColumn(hidden = false, Order = 7, filterable = true, title = "Category")]
        public string NameCategory { get; set; }

        [KendoColumn(hidden = true)]
        public bool IsNew { get; set; }
    }
}
