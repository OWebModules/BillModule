﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class SyncFinancialTransactionsParentToChildrenRequest
    {
        public string ParentId { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public SyncFinancialTransactionsParentToChildrenRequest(string parentId = null)
        {
            ParentId = parentId;
        }
    }
}
