﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BillModule.VoucherParser
{
    public class FieldParseSource
    {
        public Regex Pre { get; private set; }
        public Regex Main { get; private set; }
        public Regex Post { get; private set; }

        public List<Match> MatchesFind { get; private set; } = new List<Match>();
        public List<Match> MatchesPre { get; private set; } = new List<Match>();
        public List<Match> MatchesPost { get; private set; } = new List<Match>();

        public Regex RegexFind { get; private set; }
        public Regex RegexExtract { get; private set; }

        public SearchReplace SearchReplace { get; set; }

        public ResultItem<List<string>> Extract(string text, Globals globals)
        {
            var result = new ResultItem<List<string>>
            {
                ResultState = globals.LState_Error.Clone(),
                Result = new List<string>()
            };

            MatchesPre.Clear();
            MatchesPost.Clear();
               
            MatchesFind = RegexFind.Matches(text).Cast<Match>().ToList();
            foreach (Match match in MatchesFind)
            {
                if (match.Success)
                {
                    var matchExtract = RegexExtract.Match(match.Value);
                    if (matchExtract.Success)
                    {
                        var value = matchExtract.Value;
                        if (SearchReplace != null)
                        {
                            value = SearchReplace.Search.Replace(value, SearchReplace.Replace);
                        }
                        result.Result.Add(value);
                        result.ResultState = globals.LState_Success.Clone();

                        value = match.Value.Replace(matchExtract.Value, "");

                        if (Post != null)
                        {
                            var matchPost = Post.Match(value);
                            MatchesPost.Add(matchPost);
                            value = value.Replace(matchPost.Value, "");
                        }

                        var matchPre = Pre.Match(value.Trim());
                        MatchesPre.Add(matchPre);
                    }
                }
            }
            return result;
        }

        public FieldParseSource(string pre, string main, string post)
        {
            RegexFind = new Regex(pre + main + post);
            Pre = new Regex(pre);
            Main = new Regex(main);
            if (!string.IsNullOrEmpty(post))
            {
                Post = new Regex(post);
            }
            RegexExtract = new Regex(main);
        }
    }

    public class SearchReplace
    {
        public Regex Search { get; private set; }
        public string Replace { get; private set; }

        public SearchReplace(string search, string replace)
        {
            Search = new Regex(search);
            Replace = replace;
        }
    }
}
