﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BillModule.VoucherParser
{
    public class KeyValueParseSource
    {
        public string Key { get; private set; }
        public string Value { get; private set; }
        public string Split { get; private set; }

        public Regex RegexFind { get; private set; }
        public Regex RegexSplitter { get; private set; }
        public Regex RegexKey { get; private set; }
        public Regex RegexValue { get; private set; }

        public SearchReplace SearchReplace { get; set; }

        public ResultItem<List<ParsedKeyValue>> Extract(string text, Globals globals)
        {
            var result = new ResultItem<List<ParsedKeyValue>>
            {
                ResultState = globals.LState_Error.Clone(),
                Result = new List<ParsedKeyValue>()
            };
               
            var matches = RegexFind.Matches(text);
            foreach (Match match in matches)
            {
                if (match.Success)
                {
                    var matchKey = RegexKey.Match(match.Value);
                    if (matchKey.Success)
                    {
                        var keyValue = matchKey.Value;
                        var value = match.Value.Replace(keyValue, "");
                        var matchSplitter = RegexSplitter.Match(value);
                        if (matchSplitter.Success)
                        {
                            value = value.Replace(matchSplitter.Value, "");
                            var matchValue = RegexValue.Match(value);
                            if (matchValue.Success)
                            {
                                value = matchValue.Value;
                                if (SearchReplace != null)
                                {
                                    value = SearchReplace.Search.Replace(value, SearchReplace.Replace);
                                }
                                result.Result.Add(new ParsedKeyValue(keyValue, value));
                                result.ResultState = globals.LState_Success.Clone();
                            }
                            
                        }
                        
                    }
                }
            }
            return result;
        }

        public KeyValueParseSource(string key, string split, string value, string end)
        {
            RegexFind = new Regex(key + split + value + end);
            RegexKey = new Regex(key);
            RegexValue = new Regex(value);
            RegexSplitter = new Regex(split);
        }
    }

    public class ParsedKeyValue
    {
        public string Key { get; private set; }
        public string Value { get; private set; }
        public ParsedKeyValue(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
