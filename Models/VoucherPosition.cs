﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class VoucherPosition
    {
        public string Position { get; set; }
        public double Quantity { get; set; }

        public string Unit { get; set; }
        public double VatRate { get; set; }
        public double Amount { get; set; }
    }
}
