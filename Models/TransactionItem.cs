﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]
    [KendoFilter(mode = "row")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With" )]
    [KendoNumberFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoDateFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class TransactionItem
    {
        [KendoColumn(hidden = true)]
        public string IdTransaction { get; set; }
        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Bezeichnung")]
        public string TransactionName { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeGross { get; set; }
        [KendoColumn(hidden = false, Order = 10, filterable = true, title = "Brutto", type = ColType.BooleanType)]
        public bool Gross { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeId { get; set; }
        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Id", type = ColType.StringType)]
        public string Id { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeToPay { get; set; }
        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Betrag", type = ColType.NumberType)]
        public double ToPay { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeTransactionDate { get; set; }
        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Datum", template = "#= TransactionDate != null ? kendo.toString(kendo.parseDate(TransactionDate), 'dd.MM.yyyy hh:mm:ss') : '' #", type = ColType.DateType)]
        public DateTime? TransactionDate { get; set; }

        [KendoColumn(hidden = true)]
        public string IdCurrency { get; set; }
        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Währung")]
        public string Currency { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAmount { get; set; }
        [KendoColumn(hidden = false, Order = 6, filterable = true, title = "Menge", type = ColType.NumberType)]
        public double Amount { get; set; }

        [KendoColumn(hidden = true)]
        public string IdUnit { get; set; }
        [KendoColumn(hidden = false, Order = 7, filterable = true, title = "Einheit")]
        public string Unit { get; set; }

        [KendoColumn(hidden = true)]
        public string IdContractor { get; set; }
        [KendoColumn(hidden = false, Order = 8, filterable = true, title = "Vertragsgeber")]
        public string Contractor { get; set; }

        [KendoColumn(hidden = true)]
        public string IdContractee { get; set; }
        [KendoColumn(hidden = false, Order = 9, filterable = true, title = "Vertragsnehmer")]
        public string Contractee { get; set; }

        [KendoColumn(hidden = true)]
        public string IdTaxRate { get; set; }
        [KendoColumn(hidden = false, Order = 11, filterable = true, title = "Steuer")]
        public string TaxRate { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Ausgabe", type = ColType.BooleanType)]
        public bool IsAusgabe { get; set; }

        private TransactionItem undoItem;


        public TransactionItem GetUndoItem()
        {
            return undoItem;
        }
        public void SetUndoItem(bool isNew = false)
        {
            if (isNew)
            {
                undoItem = new TransactionItem();
            }
            else
            {
                undoItem = (TransactionItem)this.MemberwiseClone();
            }
            
        }
    }
}
