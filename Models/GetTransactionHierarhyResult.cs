﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class GetTransactionHierarhyResult
    {
        public List<clsOntologyItem> FinancialTransactions { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> FinancialTransactionsHierarchy { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> FinancialTransactionsAttributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> FinancialTransactionsRelations { get; set; } = new List<clsObjectRel>();
    }
}
