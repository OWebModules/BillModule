﻿using BillModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BillModule.VoucherParser
{
    public class ReweParser : IVoucherParser
    {
        public string IdParser { get; private set; }

        public string NameParser => "REWE";

        public bool IsResponsible(string text)
        {
            var regexRewe = new Regex("REWE.*Markt.*GmbH");
            return regexRewe.Match(text).Success;
        }

        public async Task<ResultItem<Voucher>> ParseVoucher(string text, Globals globals)
        {
            return await ParseVoucher(null, text, globals);
        }
        public async Task<ResultItem<Voucher>> ParseVoucher(string idReference, string text, Globals globals)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<Voucher>
                {
                    ResultState = globals.LState_Success,
                    Result = new Voucher { IdReference = idReference }
                };

                var lines = text.Split('\r', '\n').ToList<string>();

                var fpsDatum = new FieldParseSource(@"Datum:\s", @"\d{2}.\d{2}.\d{4}", "");
                var fpsTime = new FieldParseSource(@"Uhrzeit:\s", @"\d{2}:\d{2}:\d{2}", "");
                var fpsVatRates = new KeyValueParseSource(@"(A|B|C)", @"=\s", @"\d+,\d+", "%");
                fpsVatRates.SearchReplace = new SearchReplace(",", ".");

                var sbError = new StringBuilder();
                var success = true;

                var dateParseResult = fpsDatum.Extract(text, globals);
                if (dateParseResult.ResultState.GUID == globals.LState_Error.GUID)
                {
                    success = false;
                    sbError.AppendLine("No Date can be found!");
                }

                var timeParseResult = fpsTime.Extract(text, globals);
                if (timeParseResult.ResultState.GUID == globals.LState_Error.GUID)
                {
                    success = false;
                    sbError.AppendLine("No Time can be found!");
                }

                var vatRateParseResult = fpsVatRates.Extract(text, globals);
                if (vatRateParseResult.ResultState.GUID == globals.LState_Error.GUID)
                {
                    success = false;
                    sbError.AppendLine("No Vat Rates can be found!");
                }

                if (!success)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = sbError.ToString();
                    return result;
                }

                result.Result.DateStamp = DateTime.Parse($"{dateParseResult.Result.First()} {timeParseResult.Result.First()}");
                result.Result.Name = result.Result.DateStamp.ToString();

                var fpsPosition = new FieldParseSource(".*", @"\d+,\d+", $@"\s({string.Join("|", vatRateParseResult.Result.Select(vatRate => vatRate.Key))})");
                fpsPosition.SearchReplace = new SearchReplace(",", ".");

                var fpsAmount1 = new FieldParseSource("", @"\d+", @"\sStk");
                fpsAmount1.SearchReplace = new SearchReplace(",", ".");
                var fpsAmount2 = new FieldParseSource(".*", @"\d+,\d+", @"\skg");
                fpsAmount2.SearchReplace = new SearchReplace(",", ".");

                VoucherPosition lastPosition = null;
                foreach (var line in lines)
                {
                    if (line.Contains("--------------------------------------"))
                    {
                        break;
                    }
                    var amount1Result = fpsAmount1.Extract(line, globals);
                   
                    if (amount1Result.ResultState.GUID == globals.LState_Success.GUID && lastPosition != null)
                    {
                        lastPosition.Quantity = double.Parse(amount1Result.Result.First(), System.Globalization.CultureInfo.InvariantCulture);
                        lastPosition.Unit = "Stück";
                    }
                    else
                    {
                        var amount2Result = fpsAmount2.Extract(line, globals);
                        if (amount2Result.ResultState.GUID == globals.LState_Success.GUID && lastPosition != null)
                        {
                            lastPosition.Quantity = double.Parse(amount2Result.Result.First(), System.Globalization.CultureInfo.InvariantCulture);
                            lastPosition.Unit = "kg";
                        }
                    }

                    
                    var positionResult = fpsPosition.Extract(line, globals);
                    if (positionResult.ResultState.GUID == globals.LState_Success.GUID)
                    {
                        lastPosition = new VoucherPosition();
                        lastPosition.Position = fpsPosition.MatchesPre.First().Value;
                        lastPosition.VatRate = double.Parse(vatRateParseResult.Result.First(vr => vr.Key == fpsPosition.MatchesPost.Last().Value.Trim()).Value, System.Globalization.CultureInfo.InvariantCulture);
                        lastPosition.Amount = double.Parse(positionResult.Result.First(), System.Globalization.CultureInfo.InvariantCulture);
                        result.Result.Positions.Add(lastPosition);
                    }
                }
                

                return result;
            });

            return taskResult;
        }

        public ReweParser()
        {
            IdParser = Guid.NewGuid().ToString();
        }
    }
}
