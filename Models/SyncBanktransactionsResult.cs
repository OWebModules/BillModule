﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class SyncBanktransactionsResult
    {
        public List<clsAppDocuments> Documents { get; set; }
        public List<clsOntologyItem> TransactionItems { get; set; } = new List<clsOntologyItem>();
    }
}
