﻿using BillModule.Models;
using BillModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Factories
{
    public class TransactionItemsFactory
    {

        public async Task<ResultItem<List<TransactionItem>>> CreateTransactionItemList(TransactionItemsRaw rawItems, DateRange dateRange, Globals globals, BaseConfig baseConfig)
        {
            var taskResult = await Task.Run<ResultItem<List<TransactionItem>>>(() =>
            {
                var result = new ResultItem<List<TransactionItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                if (rawItems.RefItem.GUID_Parent == Config.LocalData.Class_Financial_Transaction.GUID)
                {
                    rawItems.Mandant = rawItems.TransactionContractees.Where(contr => baseConfig.BaseConfigToMandanten.Any(mand => mand.ID_Other == contr.ID_Other)).Select(mand => new clsOntologyItem
                    {
                        GUID = mand.ID_Other,
                        Name = mand.Name_Other,
                        GUID_Parent = mand.ID_Parent_Other,
                        Type = globals.Type_Object
                    }).FirstOrDefault();

                    if (rawItems.Mandant == null)
                    {
                        rawItems.Mandant = rawItems.TransactionContractors.Where(contr => baseConfig.BaseConfigToMandanten.Any(mand => mand.ID_Other == contr.ID_Other)).Select(mand => new clsOntologyItem
                        {
                            GUID = mand.ID_Other,
                            Name = mand.Name_Other,
                            GUID_Parent = mand.ID_Parent_Other,
                            Type = globals.Type_Object
                        }).FirstOrDefault();
                    }
                }

                var menge = (from mengeItem in rawItems.TransactionsToAmounts
                             join mengeWert in rawItems.AmountsToAmounts on mengeItem.ID_Other equals mengeWert.ID_Object into mengeWerte
                             from mengeWert in mengeWerte.DefaultIfEmpty()
                             join mengeUnit in rawItems.AmountsToUnits on mengeItem.ID_Other equals mengeUnit.ID_Object into mengeUnits
                             from mengeUnit in mengeUnits.DefaultIfEmpty()
                             select new
                             {
                                 IdTransaction = mengeItem.ID_Object,
                                 IdMenge = mengeItem.ID_Other,
                                 Menge = mengeItem.Name_Other,
                                 IdAttributeMenge = mengeWert?.ID_Attribute,
                                 MengeWert = mengeWert?.Val_Double,
                                 IdUnit = mengeUnit?.ID_Other,
                                 NameUnit = mengeUnit?.Name_Other
                             });

                result.Result = (from trans in rawItems.TransactionItems
                                 join gross in rawItems.Gross on trans.GUID equals gross.ID_Object into grosses
                                 from gross in grosses.DefaultIfEmpty()
                                 join toPay in rawItems.ToPay on trans.GUID equals toPay.ID_Object into toPays
                                 from toPay in toPays.DefaultIfEmpty()
                                 join transactionDate in rawItems.TransactionDates on trans.GUID equals transactionDate.ID_Object into transactionDates
                                 from transactionDate in transactionDates.DefaultIfEmpty()
                                 where transactionDate != null ? rawItems.RefItem.GUID_Parent == Config.LocalData.Class_Partner.GUID ? transactionDate.Val_Date >= dateRange.Start && transactionDate.Val_Date <= dateRange.End : 1 == 1 : 1 == 1
                                 join transactionId in rawItems.TransactionIds on trans.GUID equals transactionId.ID_Object into transactionIds
                                 from transactionId in transactionIds.DefaultIfEmpty()
                                 join currency in rawItems.TransactionsToCurrencies on trans.GUID equals currency.ID_Object into currencies
                                 from currency in currencies.DefaultIfEmpty()
                                 join taxrate in rawItems.TransactionTaxRates on trans.GUID equals taxrate.ID_Object into taxrates
                                 from taxrate in taxrates.DefaultIfEmpty()
                                 join mengeItm in menge on trans.GUID equals mengeItm.IdTransaction into mengeItms
                                 from mengeItm in mengeItms.DefaultIfEmpty()
                                 join contractor in rawItems.TransactionContractors on trans.GUID equals contractor.ID_Object into contractors
                                 from contractor in contractors.DefaultIfEmpty()
                                 join contractee in rawItems.TransactionContractees on trans.GUID equals contractee.ID_Object into contractees
                                 from contractee in contractees.DefaultIfEmpty()
                                 // Rest
                                 select new TransactionItem
                                 {
                                     IdTransaction = trans.GUID,
                                     TransactionName = trans.Name,
                                     IdAttributeGross = gross?.ID_Attribute,
                                     Gross = (gross?.Val_Bit) ?? false,
                                     IdAttributeToPay = toPay?.ID_Attribute,
                                     ToPay = toPay != null ? toPay.Val_Double.Value : 0,
                                     IdAttributeTransactionDate = transactionDate?.ID_Attribute,
                                     TransactionDate = transactionDate != null ? transactionDate.Val_Date.Value : (DateTime?)null,
                                     IdAttributeId = transactionId?.ID_Attribute,
                                     Id = transactionId?.Val_String,
                                     IdCurrency = currency?.ID_Other,
                                     Currency = currency?.Name_Other,
                                     IdTaxRate = taxrate?.ID_Other,
                                     TaxRate = taxrate?.Name_Other,
                                     IdAmount = mengeItm != null ? mengeItm.IdMenge : null,
                                     Amount = mengeItm != null && mengeItm.MengeWert.HasValue ? mengeItm.MengeWert.Value : 0.0,
                                     IdUnit = mengeItm != null ? mengeItm.IdUnit : null,
                                     Unit = mengeItm != null ? mengeItm.NameUnit : null,
                                     IdContractor = contractor?.ID_Other,
                                     Contractor = contractor?.Name_Other,
                                     IdContractee = contractee?.ID_Other,
                                     Contractee = contractee?.Name_Other,
                                     IsAusgabe = contractor != null && rawItems.Mandant != null ? contractor.ID_Other == rawItems.Mandant.GUID ? false : true : true
                                 }).ToList();

                result.Result.ForEach(transItem => transItem.SetUndoItem());
                return result;
            });

            return taskResult;
        }
    }
}
