﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class eBon
    {
        public string FileName { get; set; }
        public DateTime Timestamp { get; set; }
        public double SummaryAmount { get; set; }
    }

    public class eBonPosition
    {
        public string Position { get; set; }
        public double Amount { get; set; }
        public int LineNumber { get; set; }
    }
}
