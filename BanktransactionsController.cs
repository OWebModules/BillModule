﻿using BillModule.Models;
using ElasticSearchNestConnector;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule;

namespace BillModule
{
    public class BanktransactionsController : AppController
    {

        public string FieldAuftragskonto => "Auftragskonto";
        public string FieldBuchungstag => "Buchungstag";
        public string FieldValutatag => "Valutatag";
        public string FieldBuchungstext => "Buchungstext";
        public string FieldVerwendungszweck => "Verwendungszweck";
        public string FieldBegZahl => "BegZahl";
        public string FieldGegenkonto => "Gegenkonto";
        public string FieldBLZ => "BLZ";
        public string FieldBetrag => "Betrag";
        public string FieldWaehrung => "Waehrung";
        public string FieldInfo => "Info";
        public string FieldEntityName => "EntityName";
        public string FieldImportDateStamp => "ImportDateStamp";
        public string FieldZahlungsausgang => "Zahlungsausgang";

        public async Task<ResultItem<SyncBanktransactionsResult>> SyncBanktransactions(SyncBanktransactionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncBanktransactionsResult>>(async () =>
           {
               var result = new ResultItem<SyncBanktransactionsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new SyncBanktransactionsResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               if (string.IsNullOrEmpty(request.IdTextParser))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Textparser-id is not valid!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (!Globals.is_GUID(request.IdTextParser))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Textparser-id is not GUID!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Validated request.");

               var textParserController = new TextParserController(Globals);

               request.MessageOutput?.OutputInfo("Get Textparser OItem...");
               var getTextParserOItemResult = await textParserController.GetOItem(request.IdTextParser, Globals.Type_Object);

               result.ResultState = getTextParserOItemResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Textparser (OItem) could not be found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have Textparser OItem.");

               request.MessageOutput?.OutputInfo("Get Textparser...");
               var getTextParserResult = await textParserController.GetTextParser(getTextParserOItemResult.Result);

               result.ResultState = getTextParserResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Textparser could not be found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var textParser = getTextParserResult.Result.FirstOrDefault();

               if (textParser == null)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Textparser could not be found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have Textparser:{textParser.NameParser}");

               request.MessageOutput?.OutputInfo("Get Fields...");
               var getTextParserFieldsResult = await textParserController.GetParserFields(new OntologyClasses.BaseClasses.clsOntologyItem
               {
                   GUID = textParser.IdFieldExtractor,
                   Name = textParser.NameFieldExtractor,
                   GUID_Parent = textParser.IdClassFieldExtractor,
                   Type = Globals.Type_Object
               });

               result.ResultState = getTextParserFieldsResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Fields!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have {getTextParserFieldsResult.Result.Count} fields");
               foreach (var field in getTextParserFieldsResult.Result)
               {
                   request.MessageOutput?.OutputInfo(field.NameField);
               }


               request.MessageOutput?.OutputInfo($"Connect to Index {textParser.NameIndexElasticSearch}");
               clsUserAppDBSelector dbReader = null;
               try
               {
                   dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);
               }
               catch (Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Error while connecting to Index: {ex.Message}";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Connected to Index");

               request.MessageOutput?.OutputInfo("Get Documents...");
               var page = 0;
               long count = 0;
               var readNext = true;
               var documentsResult = dbReader.GetData_Documents(1000, textParser.NameIndexElasticSearch.ToLower(), textParser.NameEsType, request.Query, page);

               if (!documentsResult.IsOK)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Error while getting documents: {documentsResult.ErrorMessage}";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (documentsResult.Documents.Any())
               {
                   var saveResult = await SaveBanktransactions(documentsResult.Documents);
                   result.ResultState = saveResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   result.Result.TransactionItems = saveResult.Result;
                   result.Result.Documents = (from doc in documentsResult.Documents
                                    join obj in saveResult.Result on doc.Dict["elasticid"].ToString() equals obj.Additional1
                                    select doc).ToList();
               }

               readNext = documentsResult.TotalCount > 1000;
               count = documentsResult.Documents.Count;
               request.MessageOutput?.OutputInfo($"{documentsResult.Documents.Count} Documents...");
               while (readNext)
               {
                   page++;
                   documentsResult = dbReader.GetData_Documents(1000, textParser.NameIndexElasticSearch, textParser.NameEsType, request.Query, page, scrollId: documentsResult.ScrollId);
                   if (!documentsResult.IsOK)
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = $"Error while getting documents: {documentsResult.ErrorMessage}";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   if (documentsResult.Documents.Any())
                   {
                       var saveResult = await SaveBanktransactions(documentsResult.Documents);
                       result.ResultState = saveResult.ResultState;

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           request.MessageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }
                   }

                   count += documentsResult.Documents.Count;
                   if (count >= documentsResult.TotalCount)
                   {
                       readNext = false;
                   }
                   request.MessageOutput?.OutputInfo($"{documentsResult.Documents.Count} Documents...");
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> SaveBanktransactions(List<clsAppDocuments> documents)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               if (!documents.Any()) return result;

               var documentsToSavePre = documents.Where(doc => doc.Dict.ContainsKey(FieldAuftragskonto)).ToList();
               var documentsSaved = new List<clsAppDocuments>();
               var valutaDatums = new List<clsObjectAtt>();

               var searchBankTransactions = documentsToSavePre.Select(doc => new clsOntologyItem
               {
                   Name = doc.Dict[FieldVerwendungszweck].ToString(),
                   GUID_Parent = BankTransactions.Config.LocalData.Class_Bank_Transaktionen.GUID
               }).ToList();

               var dbReaderBankTransactions = new OntologyModDBConnector(Globals);

               result.ResultState = dbReaderBankTransactions.GetDataObjects(searchBankTransactions);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Banktransactions!";
                   return result;
               }

               var searchValutadatums = dbReaderBankTransactions.Objects1.Select(obj => new clsObjectAtt
               {
                   ID_AttributeType = BankTransactions.Config.LocalData.AttributeType_Valutadatum.GUID,
                   ID_Object = obj.GUID
               }).ToList();

               var dbReaderValutadatums = new OntologyModDBConnector(Globals);

               if (searchValutadatums.Any())
               {
                   result.ResultState = dbReaderValutadatums.GetDataObjectAtt(searchValutadatums);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Valutadatums!";
                       return result;
                   }

               }

               var searchBetraege = dbReaderBankTransactions.Objects1.Select(att => new clsObjectAtt
               {
                   ID_AttributeType = BankTransactions.Config.LocalData.AttributeType_Betrag.GUID,
                   ID_Object = att.GUID
               }).ToList();

               var dbReaderBetraege = new OntologyModDBConnector(Globals);

               if (searchBetraege.Any())
               {
                   result.ResultState = dbReaderBetraege.GetDataObjectAtt(searchBetraege);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Betraege!";
                       return result;
                   }
               }

               var searchGegenkontos = dbReaderBankTransactions.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = BankTransactions.Config.LocalData.ClassRel_Bank_Transaktionen_Gegenkonto_Bankkonto.ID_RelationType,
                   ID_Parent_Other = BankTransactions.Config.LocalData.ClassRel_Bank_Transaktionen_Gegenkonto_Bankkonto.ID_Class_Right
               }).ToList();

               var dbReaderGegenkontos = new OntologyModDBConnector(Globals);

               if (searchGegenkontos.Any())
               {
                   result.ResultState = dbReaderGegenkontos.GetDataObjectRel(searchGegenkontos);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Gegenkontos!";
                       return result;
                   }
               }

               var searchBegZahl = dbReaderBankTransactions.Objects1.Select(att => new clsObjectAtt
               {
                   ID_AttributeType = BankTransactions.Config.LocalData.AttributeType_Beg_nstigter_Zahlungspflichtiger.GUID,
                   ID_Object = att.GUID
               }).ToList();

               var dbReaderBegZahl = new OntologyModDBConnector(Globals);

               if (searchBegZahl.Any())
               {
                   result.ResultState = dbReaderBegZahl.GetDataObjectAtt(searchBegZahl);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Gegenkontos!";
                       return result;
                   }
               }

               var documentsToSave = new List<clsAppDocuments>();
               foreach (var doc in documentsToSavePre)
               {
                   var bankTransactions = from bankTransaction in dbReaderBankTransactions.Objects1.Where(trans => trans.Name == doc.Dict[FieldVerwendungszweck].ToString())
                                          join valutaDatum in dbReaderValutadatums.ObjAtts on bankTransaction.GUID equals valutaDatum.ID_Object
                                          join betrag in dbReaderBetraege.ObjAtts on bankTransaction.GUID equals betrag.ID_Object
                                          join begZahl in dbReaderBegZahl.ObjAtts on bankTransaction.GUID equals begZahl.ID_Object
                                          join gegenKonto in dbReaderGegenkontos.ObjectRels on bankTransaction.GUID equals gegenKonto.ID_Object into gegenKontos
                                          from gegenKonto in gegenKontos.DefaultIfEmpty()
                                          select new { bankTransaction, valutaDatum, betrag, begZahl, gegenKonto };

                   bankTransactions = bankTransactions.Where(trans => (DateTime)doc.Dict[FieldValutatag] == trans.valutaDatum.Val_Date &&
                        (double)doc.Dict[FieldBetrag] == trans.betrag.Val_Double &&
                        doc.Dict[FieldBegZahl].ToString() == trans.begZahl.Val_String).ToList();

                   if (doc.Dict.ContainsKey(FieldGegenkonto) && !string.IsNullOrEmpty(doc.Dict[FieldGegenkonto].ToString()))
                   {
                       bankTransactions = bankTransactions.Where(bankT => bankT.gegenKonto != null && bankT.gegenKonto.Name_Other == doc.Dict[FieldGegenkonto].ToString()).ToList();
                   }


                   if (!bankTransactions.Any())
                   {
                       documentsToSave.Add(doc);
                   }
               }

               var transactionsToSave = documentsToSave.Select(doc =>
               {
                   var trans = new clsOntologyItem
                   {
                       GUID = Globals.NewGUID,
                       Name = doc.Dict[FieldVerwendungszweck].ToString(),
                       GUID_Parent = BankTransactions.Config.LocalData.Class_Bank_Transaktionen.GUID,
                       Type = Globals.Type_Object,
                       Additional1 = doc.Dict["elasticid"].ToString()
                   };

                   return new { doc, trans };
               }).ToList();

               if (transactionsToSave.Any())
               {
                   var objectsToSave = documentsToSave.GroupBy(doc => doc.Dict[FieldAuftragskonto].ToString()).Select(doc => new clsOntologyItem
                   {
                       Name = doc.Key,
                       GUID_Parent = BankTransactions.Config.LocalData.Class_Bankkonto.GUID,
                       Type = Globals.Type_Object
                   }).ToList();

                   objectsToSave.AddRange(documentsToSave.Where(doc => !string.IsNullOrEmpty(doc.Dict[FieldGegenkonto].ToString())).GroupBy(doc => doc.Dict[FieldGegenkonto].ToString()).Select(doc => new clsOntologyItem
                   {
                       Name = doc.Key,
                       GUID_Parent = BankTransactions.Config.LocalData.Class_Bankkonto.GUID,
                       Type = Globals.Type_Object
                   }));

                   objectsToSave.AddRange(documentsToSave.GroupBy(doc => doc.Dict[FieldWaehrung].ToString()).Select(doc => new clsOntologyItem
                   {
                       Name = doc.Key,
                       GUID_Parent = BankTransactions.Config.LocalData.Class_Currencies.GUID,
                       Type = Globals.Type_Object
                   }));

                   var elasticAgent = new Services.ElasticServiceAgentTransaction(Globals);

                   var checkResultKontos = await elasticAgent.CheckOrCreateObjectsByName(objectsToSave);

                   result.ResultState = checkResultKontos.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   var saveResult = await elasticAgent.SaveObjects(transactionsToSave.Select(trans => trans.trans).ToList());

                   result.ResultState = saveResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the Transactions!";
                       return result;
                   }

                   var relationConfig = new clsRelationConfig(Globals);

                   var attributes = new List<clsObjectAtt>();
                   var relations = new List<clsObjectRel>();
                   transactionsToSave.Where(trans => trans.doc.Dict.ContainsKey(FieldValutatag)).ToList().ForEach(trans =>
                   {
                       var auftragskonto = objectsToSave.FirstOrDefault(obj => obj.GUID_Parent == BankTransactions.Config.LocalData.Class_Bankkonto.GUID && obj.Name == trans.doc.Dict[FieldAuftragskonto].ToString());

                       relations.Add(relationConfig.Rel_ObjectRelation(trans.trans, auftragskonto, BankTransactions.Config.LocalData.RelationType_Auftragskonto));

                       if (trans.doc.Dict.ContainsKey(FieldGegenkonto) && !string.IsNullOrEmpty(trans.doc.Dict[FieldGegenkonto].ToString()))
                       {
                           var gegenKonto = objectsToSave.FirstOrDefault(obj => obj.GUID_Parent == BankTransactions.Config.LocalData.Class_Bankkonto.GUID && obj.Name == trans.doc.Dict[FieldGegenkonto].ToString());

                           relations.Add(relationConfig.Rel_ObjectRelation(trans.trans, gegenKonto, BankTransactions.Config.LocalData.RelationType_Gegenkonto));
                       }

                       var currency = objectsToSave.FirstOrDefault(obj => obj.GUID_Parent == BankTransactions.Config.LocalData.Class_Currencies.GUID && obj.Name == trans.doc.Dict[FieldWaehrung].ToString());

                       relations.Add(relationConfig.Rel_ObjectRelation(trans.trans, currency, BankTransactions.Config.LocalData.RelationType_belonging));

                       attributes.Add(relationConfig.Rel_ObjectAttribute(trans.trans, BankTransactions.Config.LocalData.AttributeType_Beg_nstigter_Zahlungspflichtiger, trans.doc.Dict[FieldBegZahl].ToString()));
                       attributes.Add(relationConfig.Rel_ObjectAttribute(trans.trans, BankTransactions.Config.LocalData.AttributeType_Info, trans.doc.Dict[FieldInfo]));
                       attributes.Add(relationConfig.Rel_ObjectAttribute(trans.trans, BankTransactions.Config.LocalData.AttributeType_Buchungstext, trans.doc.Dict[FieldBuchungstext]));
                       if (trans.doc.Dict.ContainsKey(FieldBuchungstag))
                       {
                           attributes.Add(relationConfig.Rel_ObjectAttribute(trans.trans, BankTransactions.Config.LocalData.AttributeType_Buchungstag, trans.doc.Dict[FieldBuchungstag]));
                       }
                       
                       attributes.Add(relationConfig.Rel_ObjectAttribute(trans.trans, BankTransactions.Config.LocalData.AttributeType_Valutadatum, trans.doc.Dict[FieldValutatag]));
                       attributes.Add(relationConfig.Rel_ObjectAttribute(trans.trans, BankTransactions.Config.LocalData.AttributeType_Betrag, trans.doc.Dict[FieldBetrag]));
                       attributes.Add(relationConfig.Rel_ObjectAttribute(trans.trans, BankTransactions.Config.LocalData.AttributeType_Verwendungszweck, trans.doc.Dict[FieldVerwendungszweck]));
                   });

                   var dbWriter = new OntologyModDBConnector(Globals);

                   if (attributes.Any())
                   {
                       result.ResultState = dbWriter.SaveObjAtt(attributes);

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Save attributes!";
                           return result;
                       }
                   }

                   if (relations.Any())
                   {
                       result.ResultState = dbWriter.SaveObjRel(relations);

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Save relations!";
                           return result;
                       }
                   }
                   
               }
               result.Result = transactionsToSave.Select(trans => trans.trans).ToList();
               return result;
           });

            return taskResult;
        }

        public BanktransactionsController(Globals globals) : base(globals)
        {
        }
    }
}
