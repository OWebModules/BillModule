﻿using BillModule.Factories;
using BillModule.Models;
using BillModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule
{
    public class TransactionConnector
    {
        public Globals Globals { get; private set; }

        public clsOntologyItem ClassPartner
        {
            get
            {
                return Config.LocalData.Class_Partner;
            }
        }

        public clsOntologyItem ClassFinancialTransaction
        {
            get
            {
                return Config.LocalData.Class_Financial_Transaction;
            }
        }

        public clsOntologyItem ClassTaxRates
        {
            get
            {
                return Config.LocalData.Class_Tax_Rates;
            }
        }

        public clsOntologyItem ClassUnit
        {
            get
            {
                return Config.LocalData.Class_Einheit;
            }
        }

        public clsOntologyItem ClassCurrencies
        {
            get
            {
                return Config.LocalData.Class_Currencies;
            }
        }

        public BaseConfig BaseConfig { get; private set; }

        public async Task<ResultOItem<ClassObject>> GetOItem(string idItem)
        {
            var serviceElastic = new ElasticServiceAgentTransaction(Globals);
            return await serviceElastic.GetClassObject(idItem);
        }

        
        public async Task<ResultItem<List<TransactionItem>>> GetTransactionItems(GetTransactionsRequest request)
        {
            var result = new ResultItem<List<TransactionItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<TransactionItem>()
            };

            var serviceElastic = new ElasticServiceAgentTransaction(Globals);

            if (BaseConfig == null)
            {
                var baseConfigResult = await serviceElastic.GetBaseConfig();
                result.ResultState = baseConfigResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                BaseConfig = baseConfigResult.Result;
            }

            var rawItems = await serviceElastic.GetTransactionItems(request.RefItem);

            if (rawItems.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = rawItems.ResultState;
                return result;
            }

            var factory = new TransactionItemsFactory();

            var factoryResult = await factory.CreateTransactionItemList(rawItems.Result, request.DateRange, Globals, BaseConfig);

            return factoryResult;
        }

        public async Task<ResultItem<TransactionItem>> SaveTransactionItem(TransactionItem transactionItem)
        {
            var result = new ResultItem<TransactionItem>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = transactionItem
            };

            var elasticAgent = new Services.ElasticServiceAgentTransaction(Globals);

            if (BaseConfig == null)
            {
                var baseConfigResult = await elasticAgent.GetBaseConfig();
                result.ResultState = baseConfigResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                BaseConfig = baseConfigResult.Result;
            }

            if (transactionItem.GetUndoItem() == null)
            {
                transactionItem.SetUndoItem(true);
            }

            if (transactionItem.GetUndoItem().TransactionName != transactionItem.TransactionName)
            {
                var resultSave = await elasticAgent.SaveTransactionName(transactionItem.IdTransaction, transactionItem.TransactionName);
                if (resultSave.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = resultSave.ResultState;
                    return result;
                }

                transactionItem.IdTransaction = resultSave.Result.GUID;
                

            }
            if (transactionItem.GetUndoItem().TransactionDate != transactionItem.TransactionDate)
            {
                result.ResultState = await elasticAgent.SaveTransactionDate(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, transactionItem.TransactionDate);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }
            if (transactionItem.GetUndoItem().Id != transactionItem.Id)
            {
                result.ResultState = await elasticAgent.SaveTransactionId(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, transactionItem.Id);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

            }
            if (transactionItem.GetUndoItem().IdUnit != transactionItem.IdUnit ||
                transactionItem.GetUndoItem().Amount != transactionItem.Amount)
            {

                var saveResult = await elasticAgent.SaveTransactionAmount(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, transactionItem.Amount, transactionItem.IdUnit);

                if (saveResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = saveResult.ResultState;
                    return result;
                }

                transactionItem.IdAmount = saveResult.Result.Amount.GUID;
                transactionItem.Amount = saveResult.Result.AmountAmount.Val_Double.Value;
                transactionItem.IdUnit = saveResult.Result.Unit.GUID;
                transactionItem.Unit = saveResult.Result.Unit.Name;

            }
            if (transactionItem.GetUndoItem().IdTaxRate != transactionItem.IdTaxRate)
            {

                var saveResult = await elasticAgent.SaveTaxRate(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, transactionItem.IdTaxRate);

                if (saveResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = saveResult.ResultState;
                    return result;
                }

                transactionItem.IdTaxRate = saveResult.Result.GUID;
                transactionItem.TaxRate = saveResult.Result.Name;
                
            }
            if (transactionItem.GetUndoItem().IdCurrency != transactionItem.IdCurrency)
            {

                var saveResult = await elasticAgent.SaveCurrency(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, transactionItem.IdCurrency);

                if (saveResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = saveResult.ResultState;
                    return result;
                }

                transactionItem.IdCurrency = saveResult.Result.GUID;
                transactionItem.Currency = saveResult.Result.Name;

            }
            if (transactionItem.GetUndoItem().ToPay != transactionItem.ToPay)
            {

                var saveResult = await elasticAgent.SaveToPay(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, transactionItem.ToPay);

                if (saveResult.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = saveResult;
                    return result;
                }

                

            }
            if (transactionItem.GetUndoItem().Gross != transactionItem.Gross)
            {

                var saveResult = await elasticAgent.SaveGross(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, transactionItem.Gross);

                if (saveResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = saveResult.ResultState;
                    return result;
                }

                transactionItem.IdAttributeGross = saveResult.Result.ID_Attribute;
                transactionItem.Gross = saveResult.Result.Val_Bit.Value;



            }
            if (transactionItem.GetUndoItem().IdContractor != transactionItem.IdContractor)
            {

                var saveResult = await elasticAgent.SaveContractor(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, new clsOntologyItem
                {
                    GUID = transactionItem.IdContractor,
                    Name = transactionItem.Contractor,
                    GUID_Parent = Config.LocalData.Class_Partner.GUID,
                    Type = Globals.Type_Object
                });

                if (saveResult.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = saveResult;
                    return result;
                }

            }
            if (transactionItem.GetUndoItem().IdContractee != transactionItem.IdContractee)
            {

                var saveResult = await elasticAgent.SaveContractee(new clsOntologyItem
                {
                    GUID = transactionItem.IdTransaction,
                    Name = transactionItem.TransactionName,
                    GUID_Parent = Config.LocalData.Class_Financial_Transaction.GUID,
                    Type = Globals.Type_Object
                }, new clsOntologyItem
                {
                    GUID = transactionItem.IdContractee,
                    Name = transactionItem.Contractee,
                    GUID_Parent = Config.LocalData.Class_Partner.GUID,
                    Type = Globals.Type_Object
                });

                if (saveResult.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = saveResult;
                    return result;
                }

            }


            result.Result.SetUndoItem();

            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> SyncFinancialTransactionsParentToChildren(SyncFinancialTransactionsParentToChildrenRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                request.MessageOutput?.OutputInfo("Get Financial Transactions to sync...");

                var elasticAgent = new ElasticServiceAgentTransaction(Globals);

                var getTransactionsResult = await elasticAgent.GetTransactionsToSync(request);

                result.ResultState = getTransactionsResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Financial Transactions to sync.");

                var parentTransactions = (from transaction in getTransactionsResult.Result.FinancialTransactions
                                          join children in getTransactionsResult.Result.FinancialTransactionsHierarchy on transaction.GUID equals children.ID_Other into children1
                                          from children in children1.DefaultIfEmpty()
                                          select new { Transaction = transaction, IsParent = children == null }).ToList();

                var transactionsWithRels = (from transaction in parentTransactions
                               join gross in getTransactionsResult.Result.FinancialTransactionsAttributes
                                    .Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_gross.GUID) 
                                    on transaction.Transaction.GUID equals gross.ID_Object into grosses
                               from gross in grosses.DefaultIfEmpty()
                               join transactionDate in getTransactionsResult.Result.FinancialTransactionsAttributes
                                    .Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Transaction_Date.GUID) 
                                    on transaction.Transaction.GUID equals transactionDate.ID_Object into transactionDates
                               from transactionDate in transactionDates.DefaultIfEmpty()
                                join currency in getTransactionsResult.Result.FinancialTransactionsRelations
                                        .Where(rft => rft.ID_Parent_Other == Config.LocalData.Class_Currencies.GUID)
                                        on transaction.Transaction.GUID equals currency.ID_Object into courrencies
                                from currency in courrencies.DefaultIfEmpty()
                                join contractee in getTransactionsResult.Result.FinancialTransactionsRelations
                                    .Where(rft => rft.ID_RelationType == Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_RelationType && rft.ID_Parent_Other == Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_Class_Right)
                                    on transaction.Transaction.GUID equals contractee.ID_Object into contractees
                               from contractee in contractees.DefaultIfEmpty()
                               join contractor in getTransactionsResult.Result.FinancialTransactionsRelations
                                .Where(rft => rft.ID_RelationType == Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractor_Partner.ID_RelationType && rft.ID_Parent_Other == Config.LocalData.ClassRel_Financial_Transaction_belonging_Contractee_Partner.ID_Class_Right)
                                on transaction.Transaction.GUID equals contractor.ID_Object into contractors
                               from contractor in contractors.DefaultIfEmpty()
                               select new { transaction, gross, transactionDate, currency, contractee, contractor }).ToList();

                var relationsToDelete = new List<clsObjectRel>();
                var relationsToSave = new List<clsObjectRel>();
                var attributesToSave = new List<clsObjectAtt>();
                var relationConfig = new clsRelationConfig(Globals);

                foreach (var item in transactionsWithRels.Where(tran => tran.transaction.IsParent).ToList())
                {
                    var children = (from rel in getTransactionsResult.Result.FinancialTransactionsHierarchy.Where(rel => rel.ID_Object == item.transaction.Transaction.GUID).ToList()
                                    join child in transactionsWithRels on rel.ID_Other equals child.transaction.Transaction.GUID
                                    select child).ToList();

                    var childrenToSyncGross = children.Where(child => item.gross?.Val_Bit != child.gross?.Val_Bit).ToList();
                    var childrenToSyncTransactionDate = children.Where(child => item.transactionDate?.Val_Date != child.transactionDate?.Val_Date).ToList();
                    var childrenToSyncCurrency = children.Where(child => item.currency?.ID_Other != child.currency?.ID_Other).ToList();
                    var childrenToSyncContractor = children.Where(child => item.contractor?.ID_Other != child.contractor?.ID_Other).ToList();
                    var childrenToSyncContractee = children.Where(child => item.contractee?.ID_Other != child.contractee?.ID_Other).ToList();


                    
                    if (childrenToSyncGross.Any())
                    {
                        childrenToSyncGross.ForEach(gross =>
                        {
                            if (gross.gross != null && gross.gross.Val_Bit != item.gross.Val_Bit)
                            {
                                gross.gross.Val_Bit = item.gross.Val_Bit;
                                attributesToSave.Add(gross.gross);
                                result.Result.Add(gross.transaction.Transaction);
                            }
                            else if (gross.gross == null && item.gross != null)
                            {
                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(gross.transaction.Transaction, Config.LocalData.AttributeType_gross, item.gross.Val_Bit));
                                result.Result.Add(gross.transaction.Transaction);
                            }
                        });

                    }

                    if (childrenToSyncTransactionDate.Any())
                    {
                        childrenToSyncTransactionDate.ForEach(gross =>
                        {
                            if (gross.transactionDate != null && gross.transactionDate.Val_Bit != item.transactionDate.Val_Bit)
                            {
                                gross.transactionDate.Val_Bit = item.transactionDate.Val_Bit;
                                attributesToSave.Add(gross.transactionDate);
                                result.Result.Add(gross.transaction.Transaction);

                            }
                            else if (gross.transactionDate == null && item.transactionDate != null)
                            {
                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(gross.transaction.Transaction, Config.LocalData.AttributeType_Transaction_Date, item.transactionDate.Val_Date));
                                result.Result.Add(gross.transaction.Transaction);
                            }
                        });
                    }

                    if (childrenToSyncCurrency.Any() && item.currency != null)
                    {
                        childrenToSyncCurrency.ForEach(currency =>
                        {
                            if (currency.currency != null && 
                                (currency.currency.ID_Object != item.currency?.ID_Object ||
                                currency.currency.ID_RelationType != item.currency?.ID_RelationType ||
                                currency.currency.ID_Other != item.currency?.ID_Other))
                            {
                                relationsToDelete.Add(new clsObjectRel
                                {
                                    ID_Object = currency.currency.ID_Object,
                                    ID_RelationType = currency.currency.ID_RelationType,
                                    ID_Other = currency.currency.ID_Other
                                });

                                relationsToSave.Add(relationConfig.Rel_ObjectRelation(currency.transaction.Transaction, new clsOntologyItem
                                {
                                    GUID = item.currency.ID_Other,
                                    Name = item.currency.Name_Other,
                                    GUID_Parent = item.currency.ID_Parent_Other,
                                    Type = item.currency.Ontology
                                }, Config.LocalData.RelationType_belonging_Currency ));

                                result.Result.Add(currency.transaction.Transaction);
                            }
                            else if (currency.currency == null)
                            {
                                relationsToSave.Add(relationConfig.Rel_ObjectRelation(currency.transaction.Transaction, new clsOntologyItem
                                {
                                    GUID = item.currency.ID_Other,
                                    Name = item.currency.Name_Other,
                                    GUID_Parent = item.currency.ID_Parent_Other,
                                    Type = item.currency.Ontology
                                }, Config.LocalData.RelationType_belonging_Currency));

                                result.Result.Add(currency.transaction.Transaction);
                            }
                        });
                    }

                    if (childrenToSyncContractor.Any() && item.contractor != null)
                    {
                        childrenToSyncContractor.ForEach(contractor =>
                        {
                            if (contractor.contractor != null &&
                                (contractor.contractor.ID_Object != item.contractor?.ID_Object ||
                                contractor.contractor.ID_RelationType != item.contractor?.ID_RelationType ||
                                contractor.contractor.ID_Other != item.contractor?.ID_Other))
                            {
                                relationsToDelete.Add(new clsObjectRel
                                {
                                    ID_Object = contractor.contractor.ID_Object,
                                    ID_RelationType = contractor.contractor.ID_RelationType,
                                    ID_Other = contractor.contractor.ID_Other
                                });

                                relationsToSave.Add(relationConfig.Rel_ObjectRelation(contractor.transaction.Transaction, new clsOntologyItem
                                {
                                    GUID = item.contractor.ID_Other,
                                    Name = item.contractor.Name_Other,
                                    GUID_Parent = item.contractor.ID_Parent_Other,
                                    Type = item.contractor.Ontology
                                }, Config.LocalData.RelationType_belonging_Contractor));

                                result.Result.Add(contractor.transaction.Transaction);
                            }
                            else if (contractor.contractor == null)
                            {
                                relationsToSave.Add(relationConfig.Rel_ObjectRelation(contractor.transaction.Transaction, new clsOntologyItem
                                {
                                    GUID = item.contractor.ID_Other,
                                    Name = item.contractor.Name_Other,
                                    GUID_Parent = item.contractor.ID_Parent_Other,
                                    Type = item.contractor.Ontology
                                }, Config.LocalData.RelationType_belonging_Contractor));

                                result.Result.Add(contractor.transaction.Transaction);
                            }
                        });
                    }

                    if (childrenToSyncContractee.Any() && item.contractee != null)
                    {
                        childrenToSyncContractee.ForEach(contractee =>
                        {
                            if (contractee.contractee != null &&
                                (contractee.contractee.ID_Object != item.contractee?.ID_Object ||
                                contractee.contractee.ID_RelationType != item.contractee?.ID_RelationType ||
                                contractee.contractee.ID_Other != item.contractee?.ID_Other))
                            {
                                relationsToDelete.Add(new clsObjectRel
                                {
                                    ID_Object = contractee.contractee.ID_Object,
                                    ID_RelationType = contractee.contractee.ID_RelationType,
                                    ID_Other = contractee.contractee.ID_Other
                                });

                                relationsToSave.Add(relationConfig.Rel_ObjectRelation(contractee.transaction.Transaction, new clsOntologyItem
                                {
                                    GUID = item.contractee.ID_Other,
                                    Name = item.contractee.Name_Other,
                                    GUID_Parent = item.contractee.ID_Parent_Other,
                                    Type = item.contractee.Ontology
                                }, Config.LocalData.RelationType_belonging_Contractee));

                                result.Result.Add(contractee.transaction.Transaction);
                            }
                            else if (contractee.contractee == null)
                            {
                                relationsToSave.Add(relationConfig.Rel_ObjectRelation(contractee.transaction.Transaction, new clsOntologyItem
                                {
                                    GUID = item.contractee.ID_Other,
                                    Name = item.contractee.Name_Other,
                                    GUID_Parent = item.contractee.ID_Parent_Other,
                                    Type = item.contractee.Ontology
                                }, Config.LocalData.RelationType_belonging_Contractee));

                                result.Result.Add(contractee.transaction.Transaction);
                            }
                        });
                    }
                }

                if (relationsToDelete.Any())
                {
                    result.ResultState = await elasticAgent.DeleteRelations(relationsToDelete);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                if (attributesToSave.Any())
                {
                    result.ResultState = await elasticAgent.SaveAttributes(attributesToSave);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                if (relationsToSave.Any())
                {
                    result.ResultState = await elasticAgent.SaveRelations(relationsToSave);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public TransactionConnector(Globals globals)
        {
            Globals = globals;
        }

        public async Task<clsOntologyItem> Initialize()
        {
            var elasticAgent = new Services.ElasticServiceAgentTransaction(Globals);

            var getBaseConfigResult = await elasticAgent.GetBaseConfig();

            if (getBaseConfigResult.ResultState.GUID == Globals.LState_Success.GUID)
            {
                BaseConfig = getBaseConfigResult.Result;
            }

            return getBaseConfigResult.ResultState;
        }

    }
}
