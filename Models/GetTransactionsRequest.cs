﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillModule.Models
{
    public class GetTransactionsRequest
    {
        public clsOntologyItem RefItem { get; private set; }
        public DateRange DateRange { get; private set; }
        public bool DetailView { get; private set; } = false;

        public GetTransactionsRequest(clsOntologyItem refItem, DateRange dateRange, bool detailView = false)
        {
            RefItem = refItem;
            DateRange = dateRange;
            DetailView = detailView;
        }
    }
}
